import random
import string

import cherrypy


class StringGenerator(object):
    def __init__(self):
        self.string_list = []
    
    @cherrypy.expose
    def index(self):
        return """<html>
          <head></head>
          <body>
            <form method="get" action="generate">
              <input type="text" value="8" name="length" />
              <button type="submit">Give it now!</button>
            </form>
          </body>
        </html>"""

    @cherrypy.expose
    def generate(self, length=8):
        some_string = ''.join(random.sample(string.hexdigits, int(length)))
        self.string_list.append(some_string)
        total_string = ''
        for one_string in self.string_list:
            total_string = total_string + one_string
        print(total_string)
        cherrypy.session['mystring'] = total_string
        
        return some_string

    @cherrypy.expose
    def display(self):
        return cherrypy.session['mystring']


if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True
        }
    }
    cherrypy.quickstart(StringGenerator(), '/', conf)