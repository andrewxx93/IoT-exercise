import cherrypy
import string
import random

class HelloWorld(object):
    
    @cherrypy.expose
    def index(self):
        return "Hello world!"
    
    @cherrypy.expose
    def generate(self,length = 23):
        return ''.join(random.sample(string.hexdigits,int(length))) 
        #pay attention since the function sample return a random value among the
        #population set we pass. In this case the population set is composed by
        #all numbers from 0 to 9 and the letters from a to f and A to F for a total
        #of 22 elements in the population set. So the max length we can provide is 22


if __name__ == '__main__':
    cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(HelloWorld())