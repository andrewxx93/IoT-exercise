from functools import reduce
import json
class Calculator(object):
    def __init__(self):
        self.__response = {
            "operation": "",
            "operand list": None,
            "result" : None
        }   
        
    def add(self,op_list):
        response = self.__response
        response["operation"] = "add"
        response["operand list"] = op_list
        response["result"] = sum(op_list)
        print(json.dumps(response))
        pretty_json_print(response)
    
    def sub(self,op_list):
        response = self.__response
        response["operation"] = "add"
        response["operand list"] = op_list
        response["result"] = reduce(lambda op1,op2: op1-op2,op_list)
        print(json.dumps(response))
        pretty_json_print(response)
        
    def mul(self,op_list):
        response = self.__response
        response["operation"] = "add"
        response["operand list"] = op_list
        response["result"] = reduce(lambda op1,op2: op1*op2,op_list)
        print(json.dumps(response))
        pretty_json_print(response)
        
    def div(self,op_list):
        response = self.__response
        try:
            response["result"] = reduce(lambda op1,op2: op1/op2,op_list)
            
        except ZeroDivisionError:
            print("Ops...you're trying something crazy!!!")
        else:
            response["operation"] = "add"
            response["operand list"] = op_list
            print(json.dumps(response))
            pretty_json_print(response)
            
def pretty_json_print(json_file):
    result = "{\n"
    for k,v in json_file.items():
        result += f"\t{k}: {v}\n"
    result += "}"
    print(result)
    with open("operation.json","w") as fp:
        json.dump(json_file,fp,indent=4)

if __name__=='__main__':
    print("Welcome to the simple calculator...")
    text = "Insert the operation you want to perform followed by the operands\nadd op1 op2 op3...\nsub op1 op2 op3...\nmul op1 op2 op3...\ndiv op1 op2 op3...\nq->exit\nInsert your choice: "
    c = Calculator()
    while True:
        command_list= input(text).split(' ')
        command = command_list[0]
        op_list = command_list[1:]
        if command == "q":
            print("See you soon")
            break
        else:
            if command in ["add","sub","mul","div"]:
                try:
                    op_list = [int(elem) for elem in op_list]
                    
                except ValueError:
                    print("Insert a number!!!")
                else:
                    if command == "add":
                        c.add(op_list)
                        
                    elif command == "sub":
                        c.sub(op_list)
                        
                    elif command == "mul":
                        c.mul(op_list)
                    elif command == "div":
                        c.div(op_list)
                
            
            else:
                print("Command not valid")