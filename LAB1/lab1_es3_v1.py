import json
import datetime as dt

class DeviceManager(object):
    def __init__(self):
        with open("catalog.json") as fp:
            self.catalog_content = json.load(fp)
            self.catalog_devices = self.catalog_content["devicesList"]  #Carico tutti i dispositivi del catalogo in una lista su cui vengono fatte tutte le operazioni
    
    def searchByName(self,name):
        #searching the device using the input name
        found_devices = []
        for i in range(len(self.catalog_devices)):
            if self.catalog_devices[i]["deviceName"] == name:
                found_devices.append(self.catalog_devices[i])
        if found_devices != []:
            print(f"\n\nThere are {len(found_devices)} devices with the name: '{name}'\n\n")
            for i in range(len(found_devices)):
                print('-------------------------------------')    
                print("\n".join("{}:\t{}\n".format(k, v) for k, v in found_devices[i].items()))
                print("\n")
                
                #print(json.dumps(found_devices, indent=4, sort_keys=True))   #another way to print the content of the 
                                                                              # dictionary in nicely way
        else:
            print(f"No such devices found with the name: '{name}'")
    
    def searchByID(self,ID):
        #searching the device using the input ID. For a given ID only one device could be present in the catalog!!!
        found_device = []
        save_i = -1 
        print(f"Lunghezza device list in searchById: {len(self.catalog_devices)}")
        for i in range(len(self.catalog_devices)):
            if self.catalog_devices[i]["deviceID"] == ID:
                save_i = i
                found_device.append(self.catalog_devices[save_i])
        if found_device != []:
            print(f"The device with the ID: {ID} is found")
            print("\n".join("{}:\t{}\n".format(k, v) for k, v in found_device[0].items()))
            #print(json.dumps(found_devices, indent=4, sort_keys=True))   #another way to print the content of the dictionary in nicely way
            #return self.catalog_devices[save_i] 
        return save_i                                          
            
    def searchByService(self,service):
        #searching the device using the input ID. For a given ID only one device could be present in the catalog!!!
        found_devices = []
        for i in range(len(self.catalog_devices)):
            for j in range(len(self.catalog_devices[i]["servicesDetails"])):
                if self.catalog_devices[i]["servicesDetails"][j]["serviceType"] == service:
                    found_devices.append(self.catalog_devices[i])
        if found_devices != []:
            print(f"\n\nThere are {len(found_devices)} devices with the service: '{service}'\n\n")
            for i in range(len(found_devices)):    
                print("\n".join("{}:\t{}\n".format(k, v) for k, v in found_devices[i].items()))
                #print(json.dumps(found_devices, indent=4, sort_keys=True))   #another way to print the content of the 
                                                                              # dictionary in nicely way
        else:
            print(f"No such devices found with the service: '{service}'")
            
    def searchByMeasureType(self,mesType):
        #searching the device using the input ID. For a given ID only one device could be present in the catalog!!!
        found_devices = []
        for i in range(len(self.catalog_devices)):
            for j in range(len(self.catalog_devices[i]["measureType"])):
                if self.catalog_devices[i]["measureType"][j] == mesType:
                    found_devices.append(self.catalog_devices[i])
        if found_devices != []:
            print(f"\n\nThere are {len(found_devices)} devices with the measure type: '{mesType}'\n\n")
            for i in range(len(found_devices)):    
                print("\n".join("{}:\t{}\n".format(k, v) for k, v in found_devices[i].items()))
                #print(json.dumps(found_devices, indent=4, sort_keys=True))   #another way to print the content of the 
                                                                              # dictionary in nicely way
        else:
            print(f"No such devices found with the measure type: '{mesType}'")
    
    def insert_update_device(self):
        dev_id = int(input("Insert the device id: "))
        dm = DeviceManager()
        result = dm.searchByID(dev_id)
        if result == -1:
            print("Adding a new device.........")
            dev_name = input("Insert the device name: ")
            dev_measure_type = input("Insert all the measure type separated by a space: ").split(' ')
            dev_availableServices= input("Insert all the services provided by the device separated by a space(REST o MQTT only): ").upper().split(' ')
            
            service_details_list=[]
            for elem in dev_availableServices:
                dev_ip = input("Insert the ip addres or the URL of the device: ")
                if elem == "REST":
                    print("rest web service")
                    d = {"serviceType":elem,"serviceIP":dev_ip}
                    service_details_list.append(d)
                    print(service_details_list)
                if elem == "MQTT":
                    print("MQTT service")
                    dev_topics_list = input("Insert all topics separated by a space: ").split(' ')
                    d = {"serviceType":elem,"serviceIP":dev_ip,"topic":dev_topics_list}
                    service_details_list.append(d)
                    print(service_details_list)
                    
            
            print("New device adding in progress....")
            n = NewDevice()
            print("Nuovo Device creato")
            print(type(n.add_new_device(dev_id,dev_name,dev_measure_type,dev_availableServices,service_details_list)))
            print(n.add_new_device(dev_id,dev_name,dev_measure_type,dev_availableServices,service_details_list))
            self.catalog_devices.append(n.add_new_device(dev_id,dev_name,dev_measure_type,dev_availableServices,service_details_list))
            print(len(self.catalog_devices))
            
        else:
            print(f"Updating device id {dev_id}")
            new_measure_type = input("Insert new measures type separated by space: ").split(' ')
            if new_measure_type != ['']: 
                for elem in new_measure_type:
                    self.catalog_devices[result]["measureType"].append(elem)
                    #result["measureType"].append(elem)
            new_available_service = input("Insert the new services available separated by a space: ").upper().split(' ')
            
            
            if new_available_service != ['']:
                for elem in new_available_service:
                    if elem in self.catalog_devices[result]["availableServices"]:
                        print("Service already present")
                    else:
                        print("Adding new service....")
                        self.catalog_devices[result]["availableServices"].append(elem)
                        dev_ip = input("Insert the ip addres or the URL of the device: ")
                        if elem == "REST":
                            print("rest web service")
                            d = {"serviceType":elem,"serviceIP":dev_ip}
                            self.catalog_devices[result]["servicesDetails"].append(d)
                        if elem == "MQTT":
                            print("MQTT service")
                            dev_topics_list = input("Insert all topics separated by a space: ").split(' ')
                            d = {"serviceType":elem,"serviceIP":dev_ip,"topic":dev_topics_list}
                            self.catalog_devices[result]["servicesDetails"].append(d)
        print(f"Uscita funzione update/insert. Lunghezza device list: {len(self.catalog_devices)}")                        
                        
            
    def print_catalog(self):
        print('-------------------------------------\n'
              f'owner: {self.catalog_content["projectOwner"]}\n'
              f'project name: {self.catalog_content["projectName"]}\n'
              f'last update: {self.catalog_content["lastUpdate"]}\n'
              '-------------------------------------\n'
              )
        for elem in self.catalog_content["devicesList"]:
            print("-------------------------------------\n")
            for k,v in elem.items():
                print("{}:\t{}\n".format(k,v))
            print("-------------------------------------\n")
            
    def quit(self):
        pass

class NewDevice(object):
    def add_new_device(self,dev_id,dev_name,dev_measure_type,dev_services,dev_services_details):
        return {"deviceID":dev_id,
                "deviceName":dev_name,
                "measureType":dev_measure_type,
                "availableServices":dev_services,
                "servicesDetails":dev_services_details,
                "lastUpdate": dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        
        
if __name__ == '__main__':
    d = DeviceManager()
    message = """Welcome to the devices catalog manager:
            1)Search by name
            2)Search by ID 
            3)Search by service
            4)Search by measure type
            5)Insert new device/update an existing one
            6)Print all devices catalog
            7)Quit (save the modified catalog)
            8)Exit without saving
            """

while True:
    print(message)
    command = int(input("Insert your choice: "))
    if command == 1:
        name = input("Insert device name: ")
        d.searchByName(name)
    elif command == 2:
        ID = int(input("Insert device ID: "))
        d.searchByID(ID)
    elif command == 3:
        service = input("Insert device service: ")
        d.searchByService(service)
    elif command == 4:
        mesType = input("Insert device measure type: ")
        d.searchByMeasureType(mesType)
    elif command == 5:
        d.insert_update_device()
    elif command == 6:
        d.print_catalog()
    elif command == 7:
        print("Saving modifications...")
        d.quit()
    elif command == 8:
        print("No change saved!!!!")
        exit()
    else:
        print("!!!---command not found---!!!")   
   