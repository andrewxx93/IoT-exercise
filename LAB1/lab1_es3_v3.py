import json
from datetime import datetime



class DeviceManager(object):
    def __init__(self,catalog_name):
        self.__index_list = []
        self.__catalog_name = catalog_name
        self.__base_device ={
            "deviceID": None,
            "deviceName": "",
            "measureType": [],
            "availableServices": [],
            "servicesDetails": [{}],
            "lastUpdate": None
    
}
        with open(self.__catalog_name) as fp:
            self.__catalog = json.load(fp)
            
        self.__starting_timestamp = self.__catalog["lastUpdate"]   
    def print_device_info(self,index_list):
        print(f"print device {index_list}")
        for index in index_list:
            print("---------------------------------------------------------------------------\n")
            for k,v in self.__catalog["devicesList"][index].items():
                content = f"{k}\t{v}\n"
                print(content)
            print("****************************************************************************\n")
            
    def catalog_print(self):
        owner = self.__catalog["projectOwner"]
        project_name = self.__catalog["projectName"]
        last_update = self.__catalog["lastUpdate"]
        content = "{"
        content += f"\nowner: {owner}\nproject name: {project_name}\nlast update: {last_update}"
        for i in range(len(self.__catalog["devicesList"])):
            content += "---------------------------------------------------------------------------\n"
            for k,v in self.__catalog["devicesList"][i].items():
                
                content += f"\n\t{k}: {v}\n"
            content += "****************************************************************************\n"
                
                
        
        
        print(content)
        
    def search(self,search_type,search_name):
        self.__index_list = []
        if search_type =="deviceID":
            search_name = int(search_name)
        
        for i in range(len(self.__catalog["devicesList"])):
            if isinstance(self.__catalog["devicesList"][i][search_type],list):
                if search_name in self.__catalog["devicesList"][i][search_type]:
                    self.__index_list.append(i)
            elif self.__catalog["devicesList"][i][search_type] == search_name:
                self.__index_list.append(i)
        self.print_device_info(self.__index_list)
        print(self.__index_list)
        
    def insert_update_device(self,device):
        deviceID = device["deviceID"]
        self.search("deviceID",deviceID)
        print(self.__index_list)
        
        if self.__index_list== []:
            print("Adding...")
            #Adding the new device
            timestamp = datetime.now()
            device["lastUpdate"] =  timestamp.strftime("%m/%d/%Y, %H:%M:%S")
            self.__catalog["lastUpdate"] = timestamp.strftime("%m/%d/%Y, %H:%M:%S")
            self.__catalog["devicesList"].append(device)
        else:
            #Update the existing device with the specified ID 
            timestamp = datetime.now()
            print(f"Updating device with ID {deviceID} ")
            device["lastUpdate"] =  timestamp.strftime("%d/%m/%Y, %H:%M:%S")
            self.__catalog["lastUpdate"] = timestamp.strftime("%d/%m/%Y, %H:%M:%S")
            self.__catalog["devicesList"][self.__index_list.pop()]=device
            
    def quitAndSave(self):
            if self.__starting_timestamp != self.__catalog["lastUpdate"]:
                print("Saving the change...")
                with open(self.__catalog_name,"w") as fp:
                    json.dump(self.__catalog,fp,indent=4)
            else:
                print("Nothing changed")
                
            print("See you soon :D ")
            
    def new_device(self):
        device = self.__base_device
        
        deviceID = int(input("Enter device ID: "))
        deviceName = input("Enter device name: ")
        measureType = input("Enter the measure type separated by a space: ").split(' ')
        availableServices = input("Enter the available services separated by a space: ").split(' ')
        servicesDetails = []
        for service in availableServices:
            if service.upper() == "REST":
                s = {
                    "serviceType": "REST",
                    "serviceIP": input("Enter IP address or URL of the REST server: ")
                }
                servicesDetails.append(s)
            elif service.upper() == "MQTT":
                s = {
                    "serviceType": "MQTT",
                    "serviceIP": input("Enter IP address or URL of the BROKER: "),
                    "topic" : []
                }
                command =""    
                while command != 'e':
                    command = input("Insert topic. When you finish insert 'e' to exit from the topic insert:\n")
                    if command !='e':
                        s["topic"].append(command)
                servicesDetails.append(s)
                    
        device.update({
                    
                    "deviceName": deviceName,
                    "deviceID": deviceID,
                    "measureType": measureType,
                    "availableServices": availableServices,
                    "servicesDetails": servicesDetails
                    
        })
        self.insert_update_device(device)
        return device 
if __name__ == '__main__':
    dc = DeviceManager("catalog.json")
    #dc.catalog_print()
    #dc.searchByName('DHT11')
    #dc.search("deviceName","VCNL4010")
    #dc.search("measureType","Humidity")
    #dc.search("availableServices","REST")
    #dc.catalog_print()
    
    dc.new_device()
    dc.quitAndSave()