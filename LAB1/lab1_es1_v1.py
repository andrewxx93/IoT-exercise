import json

class Calculator(object):
    def __init__(self,op_string):
        self.op_string = op_string
        op_list = self.op_string.split(" ")
        self.op = op_list[0]
        self.a = float(op_list[1])
        self.b = float(op_list[2])
        self.result = 0
        self.final_dict = {"operation" : self.op,
                            "first_operand" : self.a,
                            "second_operand" : self.b,
                            "result" : self.result
                            }
        if self.op == 'add':
            print(Calculator.add(self))
        elif self.op == 'sub':
            print(Calculator.sub(self))
        elif self.op == 'mul':
            print(Calculator.mul(self))
        elif self.op == 'div':
            print(Calculator.div(self))
        else:
            print("!!!---command not found---!!!")
            
            
    #Methods definition add = addition, sub = subtraction, mul = multiplication, div = division
    def add(self):
        self.final_dict["result"] = self.a + self.b
        with open("operation.json",'w') as fp:
            json.dump(self.final_dict,fp,indent = 4)
            
            
    def sub(self):
        self.final_dict["result"] = self.a - self.b       
        with open("operation.json",'w') as fp:
            json.dump(self.final_dict,fp,indent = 4)
            
            
    def mul(self):
        self.final_dict["result"] = self.a * self.b       
        with open("operation.json",'w') as fp:
            json.dump(self.final_dict,fp,indent = 4)
    
    
    def div(self):
        try :
            res = self.a/self.b
        except :
            self.result = "Infinity"
            print("!!!---Something went wrong. It is not possible to divide a number by zero---!!!")
        else:
            self.result = res
        finally:
            self.final_dict["result"] = self.result
            with open("operation.json",'w') as fp:
                json.dump(self.final_dict,fp,indent = 4)
        
if __name__ == '__main__':
    message = """Simply calculator.
The available commands are:
1) addition (add)
2) subtraction (sub)
3) multiplication (mul)
4) division (div)
5) quit to exit from program
Write the command as below:

add 12 4.6
sub 3 12
quit

"""

    while True:
        print(message)
        input_string = input("Insert operation:  ")
        input_command = input_string.split(" ")[0] #Il metodo split ritorna una lista e prendo il primo elemento della lista che indica l'operazione
        if input_command != 'quit':
            Calculator(input_string)
        else:
            break
        
        