import json

class Calculator(object):
    def __init__(self):
        self.__response = {
            "operation": "",
            "op1": None,
            "op2": None,
            "result" : None
        }   
        
    def add(self,op1,op2):
        response = self.__response
        response["operation"] = "add"
        response["op1"] = op1
        response["op2"] = op2
        response["result"] = op1+op2
        print(json.dumps(response))
        pretty_json_print(response)
    
    def sub(self,op1,op2):
        response = self.__response
        response["operation"] = "add"
        response["op1"] = op1
        response["op2"] = op2
        response["result"] = op1-op2
        print(json.dumps(response))
        pretty_json_print(response)
        
    def mul(self,op1,op2):
        response = self.__response
        response["operation"] = "add"
        response["op1"] = op1
        response["op2"] = op2
        response["result"] = op1*op2
        print(json.dumps(response))
        pretty_json_print(response)
        
    def div(self,op1,op2):
        response = self.__response
        try:
            response["result"] =op1/op2
            
        except ZeroDivisionError:
            print("Ops...you're trying something crazy!!!")
        else:
            response["operation"] = "add"
            response["op1"] = op1
            response["op2"] = op2
            print(json.dumps(response))
            pretty_json_print(response)
            
def pretty_json_print(json_file):
    result = "{\n"
    for k,v in json_file.items():
        result += f"\t{k}: {v}\n"
    result += "}"
    print(result)
    with open("operation.json","w") as fp:
        json.dump(json_file,fp,indent=4)

if __name__=='__main__':
    print("Welcome to the simple calculator...")
    text = "Insert the operation you want to perform followed by the operands\nadd op1 op2\nsub op1 op2\nmul op1 op2\ndiv op1 op2\nq->exit\nInsert your choice: "
    c = Calculator()
    while True:
        command_list= input(text).split(' ')
        command = command_list[0]
        if command == "q":
            print("See you soon")
            break
        else:
            if command in ["add","sub","mul","div"]:
                try:
                    op1 = int(command_list[1])
                    op2 = int(command_list[2])
                except ValueError:
                    print("Insert a number!!!")
                else:
                    if command == "add":
                        c.add(op1,op2)
                        
                    elif command == "sub":
                        c.sub(op1,op2)
                        
                    elif command == "mul":
                        c.mul(op1,op2)
                    elif command == "div":
                        c.div(op1,op2)
                
            
            else:
                print("Command not valid")