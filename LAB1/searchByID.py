def searchByID(self,ID):
        #searching the device using the input ID. For a given ID only one device could be present in the catalog!!!
        found_device = []
        
        for i in range(len(self.catalog_devices)):
            if self.catalog_devices[i]["deviceID"] == ID:
                print(i)
                found_device.append(self.catalog_devices[i])
        if found_device != []:
            print(f"The device with the ID: {ID} is found")
            print("\n".join("\t{}\t{}\n".format(k, v) for k, v in found_device[0].items()))
            #print(json.dumps(found_devices, indent=4, sort_keys=True))   #another way to print the content of the dictionary in nicely way
            print(found_device)
            return self.catalog_devices[i]    