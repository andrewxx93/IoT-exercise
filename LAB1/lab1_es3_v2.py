import json
import datetime as dt

class DeviceManager(object):
    def __init__(self,catalog_name):
        self.catalog_name = catalog_name
        with open(self.catalog_name) as fp:
            self.catalog_content = json.load(fp)
            
    def print_device_info(self,device):
        pass

    def searchByName(self,dev_name):
        #La funzione prende in ingresso il nome di un dispositivo e lo cerca dentro la devicesList
        #Prima di cercarlo le parole sono trasformate in maiuscolo in modo da rendere l'input NOCASE SENSITIVE
        #Per stampare il risultato in maniera ordinata uso il metodo join() che ritorna una stringa composta
        #da un elemento iterabile (come le liste) e una stringa separatore. In questo caso il separatore è "\n"
        for i in range(len(self.catalog_content["devicesList"])):
            if self.catalog_content["devicesList"][i]["deviceName"].upper() == dev_name.upper():
                print("--------------------------------------------")
                print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
                print("--------------------------------------------")
                
    def searchByID(self,dev_id):
        save_index = ''
        for i in range(len(self.catalog_content["devicesList"])):
            if self.catalog_content["devicesList"][i]["deviceID"]== int(dev_id):
                print("--------------------------------------------")
                print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
                print("--------------------------------------------")
                save_index = i
        if save_index == '':
            print(f"Il dispositivo con ID: {dev_id} non esiste")
            return {}
        else:
            #return self.catalog_content["devicesList"][save_index]
            return save_index
        
    def searchByService(self,service):
        count = 0
        
        for i in range(len(self.catalog_content["devicesList"])):
            if service.upper() in self.catalog_content["devicesList"][i]["availableServices"]:
                print("--------------------------------------------")
                print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
                print("--------------------------------------------")
                count += 1
                
        if count == 0:
            print(f"No devices were found with requested service: {service}")
            
    
    def searchByMeasureType(self,measure):
        count = 0
        for i in range(len(self.catalog_content["devicesList"])):
            if measure in self.catalog_content["devicesList"][i]["measureType"]:
                print("--------------------------------------------")
                print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
                print("--------------------------------------------")
                count += 1
                
        if count == 0:
            print(f"No devices were found with requested measure type: {measure}")
            print("Please, pay attention to insert the measure type in the correct format. Only the first character must be upper case."
                f"you insert: {measure}")
    
            
    def update_insert_device(self,dev_id):
        time_update =  dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.catalog_content["lastUpdate"] = time_update
        if (save_index :=self.searchByID(dev_id) ) == {}: #walrus operator !!!
            print("L'ID non è presente quindi devo aggiungere un nuovo dispositivo")
            new_name = input("Inserisci il nome: ")
            new_measure_type = input("Inserisci i tipi di misura separati da uno spazio: ").split(' ')
            new_available_service = input("Inserisci i servizi disponibili separati da uno spazio: ").split(' ')
            new_service_details_list = []
            for service in new_available_service:
                if service.upper() == 'REST':
                    print("Inserisci informazioni per il protocollo REST")
                    rest_ip = input("Inserisci l'indirizzo ip per REST: ")
                    d = {
                        "serviceType":service.upper(),
                        "serviceIP": rest_ip
                        }
                    new_service_details_list.append(d)
                elif service.upper() == 'MQTT':
                    print("Inserisci informazioni per il protocollo MQTT")
                    mqtt_ip = input("Inserisci l'indirizzo ip per MQTT: ")
                    topic_list = input("Inserisci la lista di topic da inserire separati da uno spazio").split(' ')
                    d = {
                        "serviceType":service.upper(),
                        "serviceIP": mqtt_ip,
                        "topic": topic_list
                        }
                    new_service_details_list.append(d)     
            new_device = {
                "deviceName" : new_name,
                "deviceID" : int(dev_id),
                "measureType": new_measure_type,
                "availableServices": new_available_service,
                "servicesDetails": new_service_details_list,
                "lastUpdate": time_update
                
            }
            self.catalog_content["devicesList"].append(new_device)
        
        else:
            print(f"Aggiorno il dispositivo con ID: {dev_id}")
            print("Update measure type....")
            
            #variable initialisation
            
            measure_type_list = []
            available_service_list = []
            device_name = ""
            service_details_list =[]
            self.catalog_content["devicesList"][save_index]["lastUpdate"] = time_update
            
            device_name = input("Insert the new device name: ")
            measure_type_list= input("Insert the available measure type separated by a space: ").split(' ')
            available_service_list = input("Insert the update available services separated by a space: ").split(' ')
            
            
            #Updating name
            if device_name !="":
                self.catalog_content["devicesList"][save_index]["deviceName"] = device_name.upper()
            
            
            #updating measure type list
            if measure_type_list != [""]:
                measure_type_list = [elem.upper() for elem in measure_type_list]
                self.catalog_content["devicesList"][save_index]["measureType"] = measure_type_list
                
                
            #updating available service list
            if available_service_list != [""]:
                available_service_list = [elem.upper() for elem in available_service_list]
                
            
                #updating service detail list
                for service in available_service_list:
                    if service == 'REST':
                        print("Inserisci informazioni per il protocollo REST")
                        rest_ip = input("Inserisci l'indirizzo ip per REST: ")
                        d = {
                            "serviceType":service.upper(),
                            "serviceIP": rest_ip
                            }
                        service_details_list.append(d)
                    elif service== 'MQTT':
                        print("Inserisci informazioni per il protocollo MQTT")
                        mqtt_ip = input("Inserisci l'indirizzo ip per MQTT: ")
                        topic_list = input("Inserisci la lista di topic da inserire separati da uno spazio").split(' ')
                        d = {
                            "serviceType":service.upper(),
                            "serviceIP": mqtt_ip,
                            "topic": topic_list
                            }
                        service_details_list.append(d) 
            
            self.catalog_content["devicesList"][save_index]["availableServices"] = available_service_list
            self.catalog_content["devicesList"][save_index]["servicesDetails"] = service_details_list
            
            
            
            #self.catalog_content["devicesList"][save_index]["measureType"].append(input("Inserisci il nuovo tipo di misura: "))
            #self.catalog_content["devicesList"][save_index]["availableServices"].append(input("Inserisci il nuovo servizio disponibile: "))
            print("--------------------------------------------")
            print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][save_index].items()))
            print("--------------------------------------------")
        
    def print_catalog(self):
        print('-------------------------------------\n'
            f'owner: {self.catalog_content["projectOwner"]}\n'
            f'project name: {self.catalog_content["projectName"]}\n'
            f'last update: {self.catalog_content["lastUpdate"]}\n'
            '-------------------------------------\n'
            )
        for i in range(len(self.catalog_content["devicesList"])):
            print("--------------------------------------------")
            print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
            print("--------------------------------------------")
            
    def quit(self):
        print('-------------------------------------\n'
            f'owner: {self.catalog_content["projectOwner"]}\n'
            f'project name: {self.catalog_content["projectName"]}\n'
            f'last update: {self.catalog_content["lastUpdate"]}\n'
            '-------------------------------------\n'
            )
        for i in range(len(self.catalog_content["devicesList"])):
            print("--------------------------------------------")
            print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
            print("--------------------------------------------")
        
        print(type(self.catalog_content))
        
        with open(self.catalog_name,'w') as fp:
            json.dump(self.catalog_content,fp,indent = 4)
            
if __name__ == '__main__':
    d = DeviceManager("catalog.json")
    message = """Welcome to the devices catalog manager:
            1)Search by name
            2)Search by ID 
            3)Search by service
            4)Search by measure type
            5)Insert new device/update an existing one
            6)Print all devices catalog
            7)Quit (save the modified catalog)
            8)Exit without saving
            """

while True:
    
    print(message)
    
    command = int(input("Insert your choice: "))
    
    if command == 1:
        name = input("Insert device name: ")
        d.searchByName(name)
        
    elif command == 2:
        ID = input("Insert device ID: ")
        d.searchByID(ID)
        
    elif command == 3:
        service = input("Insert device service: ")
        d.searchByService(service)
        
    elif command == 4:
        mesType = input("Insert device measure type: ")
        d.searchByMeasureType(mesType)
    
    elif command == 5:
        ID = input("Insert device ID: ")
        d.update_insert_device(ID)
        
    elif command == 6:
        d.print_catalog()
        
    elif command == 7:
        print("Saving modifications...")
        d.quit()
        #exit()
    
    elif command == 8:
        print("No change saved!!!!")
        exit()
        
    else:
        print("!!!---command not found---!!!")   
