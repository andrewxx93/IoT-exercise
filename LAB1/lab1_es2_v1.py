import json
from functools import reduce #in modo da poter usare la funzione reduce


class Calculator(object):
    def __init__(self,op_string):
        self.op_string = op_string
        self.op_list = self.op_string.split(" ")
        print(self.op_list)
        self.op = self.op_list[0]
        self.operands_list = [int(elem) for elem in self.op_list[1:]]
        
        self.result = 0
        self.final_dict = {"operation" : self.op,
                            "operand list": self.operands_list,
                            "result" : self.result
                            }
        if self.op == 'add':
            print(Calculator.add(self))
        elif self.op == 'sub':
            print(Calculator.sub(self))
        elif self.op == 'mul':
            print(Calculator.mul(self))
        elif self.op == 'div':
            print(Calculator.div(self))
        else:
            print("!!!---command not found---!!!")
            
            
    #Methods definition add = addition, sub = subtraction, mul = multiplication, div = division
    
    #La funzione reduce reduce(fun,list) prende in ingresso una funzione e la applica a tutti gli elementi 
    #della lista list. In questo caso uso la funzione anonima lambda 
    def add(self):
        
        self.final_dict["result"] = sum(self.operands_list) #effettua la somma di tutti gli elementi della lista
        with open("operation.json",'w') as fp:
            json.dump(self.final_dict,fp,indent=4)
            
            
    def sub(self):
        
        self.final_dict["result"] = reduce(lambda x,y: x-y, self.operands_list ) #sottrae tutti i termini della lsita
        with open("operation.json",'w') as fp:
            json.dump(self.final_dict,fp,indent = 4)
            
            
    def mul(self):

        self.final_dict["result"] = reduce(lambda x,y: x*y, self.operands_list ) #moltiplica tutti i termini della lista
        with open("operation.json",'w') as fp:
            json.dump(self.final_dict,fp,indent = 4)
    
    
    def div(self):
        if 0 in self.operands_list[1:]: #Controllo a partire dal secondo elemento 
            self.final_dict["result"] = "Infinity"
            print("!!!---Something went wrong. It is not possible to divide a number by zero---!!!")
                
        else:
            self.final_dict["result"] = reduce(lambda x,y: x/y, self.operands_list )
        
        with open("operation.json",'w') as fp:
            json.dump(self.final_dict,fp,indent = 4)
        
if __name__ == '__main__':
    message = """Simply calculator.
The available commands are:
1) addition (add)
2) subtraction (sub)
3) multiplication (mul)
4) division (div)
5) type quit to exit from program
Write the command as below:

add 12 4.6
sub 3 12

"""

    while True:
        print(message)
        input_string = input("Insert operation:  ")
        input_command = input_string.split(" ")[0]
        if input_command != 'quit':
            Calculator(input_string)
        else:
            break
        
        