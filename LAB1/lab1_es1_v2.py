import json

class Calculator(object):
    
    def __init__(self,input_string):
        if input_string != "":
            self.input_list = input_string.split(' ')
            self.operation = self.input_list[0]
            self.operand_list = [float(elem) for elem in self.input_list[1:] if elem!='']
            self.dict_result ={}
                
            self.result = None
            
            
            if self.operation == "quit":
                print("See you next time")
                exit()
            else:
                if self.operation in ["add","sub","mul","div"]:
                    
                    if len(self.operand_list) == 2:
                        
                        
                        if self.operation == "add":
                            self.result = self.add(self.operand_list[0],self.operand_list[1])
                            
                        elif self.operation == "sub":
                            self.result = self.sub(self.operand_list[0],self.operand_list[1])
                            
                        elif self.operation == "mul":
                            self.result = self.mul(self.operand_list[0],self.operand_list[1])
                            
                        elif self.operation == "div":
                            self.result = self.div(self.operand_list[0],self.operand_list[1])
                        
                        
                            
                    else:
                        print("--------------------------------")
                        print("Wrong operand number. The operand must be only 2")
                        print("--------------------------------")
                else:
                    print("--------------------------------")
                    print("Unknown operation")
                    print("--------------------------------")
        else:
            print("--------------------------------")
            print("Input string cannot be empty")
            print("--------------------------------")

        if self.result != None:
            self.print_result(self.result)  
            self.save_operation()
            
        
            
            
    def add(self,a,b):
        print("Summing...")
        return a+b
        
    def sub(self,a,b):
        print("Subtracting...")
        return a-b
    
    def mul(self,a,b):
        print("Multiplying...")
        return a*b
    
    def div(self,a,b):
        print("Dividing...")
        try:
            return a/b
        except ZeroDivisionError:
            print("Ops...you are trying to divide by zero")
            return "Infinity"
        
    def print_result(self,result):
            print(result)
    
    def save_operation(self,):
        self.dict_result ={
                "Operation": self.operation,
                "First operand": self.operand_list[0],
                "Second operand": self.operand_list[1],
                "Result": self.result
                
            }
        
        with open("operation.json",'w') as fp:
            json.dump(self.dict_result,fp,indent=4)
        


if __name__ == '__main__':
    message = """Simply calculator.
The available commands are:
1) addition (add)
2) subtraction (sub)
3) multiplication (mul)
4) division (div)
5) quit to exit from program
Write the command as below:

add 12 4.6
sub 3 12
mul 3 4
div 1 4
quit

"""

    while True:
        print(message)
        input_string = input("Insert string: ")
        c = Calculator(input_string)
        
    