import cherrypy
import json


@cherrypy.expose
class RESTCalculator():
    def __init__(self):
        self.__base_response ={
            "operation" : "",
            "operand1":None,
            "operand2": None,
            "result" : None
        }
        
    def GET(self,*uri,**params):
        #print(type(params))
        if len(uri)!= 0:
            operand =uri[0]
            if len(params)== 2:
                response = self.__base_response
                try:
                    if operand in [ "add","sub","mul","div"]:
                        response.update({
                                "operation" : operand,
                                "operand1" : int(params["op1"]),
                                "operand2" : int(params["op2"])
                                
                            })
                        if operand.lower() == "add":
                            response["result"] = int(params["op1"])+ int(params["op2"])
                        
                        elif operand.lower() == "sub":
                            response["result"] = int(params["op1"]) - int(params["op2"])
                        elif operand.lower() == "mul":
                            response["result"] = int(params["op1"]) * int(params["op2"])   
                        elif operand.lower() == "div":
                            try:
                                response["result"] = int(params["op1"])/ int(params["op2"])      
                            except ZeroDivisionError:
                                print("Ops....zero division error!!!!")
                                
                    
                    else:
                        raise cherrypy.HTTPError(501,f"The Operation {operand} is not implemented")
                except ValueError:
                    print("Value errors...")
            elif len(params) == 1:
                raise cherrypy.HTTPError(400,f"The Operation needs 2 operands. Only one is passed!!!")      
            elif len(params) == 0:
                raise cherrypy.HTTPError(400,f"The Operation needs 2 operands. No one is passed!!!")
            elif len(params) > 2:
                raise cherrypy.HTTPError(400,f"The Operation needs 2 operands. More then 2 are passed!!!")
            else:
                raise cherrypy.HTTPError(500,"Something went wrong")
            return json.dumps(response)
                

if __name__=='__main__':
    conf = {
        "/": {
            "request.dispatch" : cherrypy.dispatch.MethodDispatcher(),
            "tool.session.on" : True
        }
    }
    
    cherrypy.tree.mount(RESTCalculator(),'/',conf)
    cherrypy.engine.start()
    cherrypy.engine.block()
    
    
                
            
        