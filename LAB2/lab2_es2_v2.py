import cherrypy 
import json


class RESTCalculator(object):
    def __init__(self):
        self.__base_response = {
            "operation": "",
            "operand1": None,
            "operand2": None,
            "result" : None
        }
    @cherrypy.expose    
    def index(self):
        return "Welcome !"
    @cherrypy.expose
    def add(self,*uri):
        print(len(uri))
        if len(uri)==2:
            response = self.__base_response
            response.update({
                "operation": "add",
                "operand1": int(uri[0]),
                "operand2": int(uri[1]),
                "result": int(uri[0])+int(uri[1])
            })
            return json.dumps(response,indent=4)
        else:
            raise cherrypy.HTTPError(400,f"The application requires 2 operands. {len(uri)} are passed!!! ")
        
    @cherrypy.expose
    def sub(self,*uri):
        if len(uri)==2:
            response = self.__base_response
            response.update({
                "operation": "add",
                "operand1": int(uri[0]),
                "operand2": int(uri[1]),
                "result": int(uri[0])-int(uri[1])
            })
            return json.dumps(response,indent=4)
        else:
            raise cherrypy.HTTPError(400,f"The application requires 2 operands. {len(uri)} are passed!!! ")
    
    @cherrypy.expose   
    def div(self,*uri):
        if len(uri)==2:
            try:
                response = self.__base_response
                response.update({
                    "operation": "add",
                    "operand1": int(uri[0]),
                    "operand2": int(uri[1]),
                    "result": int(uri[0])/int(uri[1])
                })
            except ZeroDivisionError:
                raise cherrypy.HTTPError(400,"You are trying to divide by zero!!!")
            return json.dumps(response,indent=4)
        else:
            raise cherrypy.HTTPError(400,f"The application requires 2 operands. {len(uri)} are passed!!! ")
        
    @cherrypy.expose    
    def mul(self,*uri):
        if len(uri)==2:
            response = self.__base_response
            response.update({
                "operation": "add",
                "operand1": int(uri[0]),
                "operand2": int(uri[1]),
                "result": int(uri[0])*int(uri[1])
            })
            return json.dumps(response,indent=4)
        else:
            raise cherrypy.HTTPError(400,f"The application requires 2 operands. {len(uri)} are passed!!! ")

if __name__ == '__main__':
    #conf = {
    #     '/': {
    #         'request.dispatch':cherrypy.dispatch.MethodDispatcher()
    #     }
    # }
    cherrypy.quickstart(RESTCalculator())
