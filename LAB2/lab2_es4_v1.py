import json
import cherrypy
import os


class DeviceRegistration:
    exposed=True
    def __init__(self,html_path,catalog_device_path):
        
        self.html_path = html_path
        self.catalog_device_path = catalog_device_path
        
        
    def GET(self):
            try:
                return open(self.html_path,'r')
            except:
                print("Error")
    
    def POST(self):
        
        #Check if the body is empty or not!!!
        print("The length of the body content is: "+ cherrypy.request.body.headers['Content-Length'])
        
        
        if int(cherrypy.request.body.headers['Content-Length']) != 0 :
            self.input = cherrypy.request.body.read()   #leggo il body 
            
            input_json = json.loads(self.input)         #converto il body in formato JSON
            
            """  #additional control
            print("The content of the body is: ")
            print(input_json)                           
            device_catalog_key = list(input_json.keys())
            if "deviceID" and"deviceName" and"measureType" and"unit" in device_catalog_key:
                
            else:
                return "<h1 style = 'text-align: center'> Ops...one or more keys are wrong or missing</h1>"
            """
            with open(self.catalog_device_path) as fp:
                device_catalog = json.load(fp)
                devices_list = device_catalog["devicesList"]
                devices_list.append(input_json)
                with open(self.catalog_device_path,'w') as fp:
                    json.dump(device_catalog, fp, indent=4)
                    
                
            
            return json.dumps(device_catalog, indent=4)
    
            
        else:
            return "<h1 style = 'text-align: center'> The Body is Empty </h1>"
        
        
if __name__ == '__main__':
        
    conf ={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tools.staticdir.root':os.path.abspath(os.getcwd()),   #setto la directory corrente come root
        },
        '/css':{
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './css'
        },
        '/js':{
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './js'
        },
    }
        
    cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(DeviceRegistration("index.html","devices.json"),'/',conf)