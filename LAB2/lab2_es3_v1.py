import json
import cherrypy
from functools import reduce #in modo da poter usare la funzione reduce


class Calculator(object):
    exposed=True
    
    def __init__(self):
            self.input =""
            self.command = ""
            self.operand_list = []
            self.dict_result ={}
            
    def PUT(self):
        
        
        #Check if the body is empty or not!!!
        print("The length of the body content is: "+ cherrypy.request.body.headers['Content-Length'])
        
        
        if int(cherrypy.request.body.headers['Content-Length']) != 0 :
            self.input = cherrypy.request.body.read()
            input_json = json.loads(self.input)
            print("The content of the body is: ")
            print(input_json)
            input_key = input_json.keys()
        
            
            if input_json == {}:
                return "<h1 style = 'text-align: center'> The dictionary in the body is Empty </h1>"
            
            #check if the key are correct or not
            elif 'command' and 'operands' in input_json.keys():
                self.command = input_json['command']
                self.operands_list = input_json['operands']
                
                #check if the operand is correct and the operands list is not empty
                if self.command in ['add', 'sub', 'div', 'mul'] and self.operands_list != [] :
                    print(self.command)
                    print(self.operands_list)
                    
                    if self.command == 'add':
                        self.add()
                        
                    elif self.command == 'sub':
                        self.sub()
                        
                    elif self.command == 'mul':
                        self.mul()
                    
                    elif self.command == 'div':
                        check_result = self.div()
                        if check_result == -1:
                            return "<h1 style = 'text-align: center'> Ops...you are trying to divide by zero</h1>"
                    
                    return json.dumps(self.dict_result,indent=4)
                    
                else:
                    return "<h1 style = 'text-align: center'> Unknown operation or Empy operands list</h1>"
            
            else:
                return "<h1 style = 'text-align: center'> Wrong key in dict </h1>"
            
        else:
            return "<h1 style = 'text-align: center'> The Body is Empty </h1>"
        
        
            
    #La funzione reduce reduce(fun,list) prende in ingresso una funzione e la applica a tutti gli elementi 
    #della lista list. In questo caso uso la funzione anonima lambda 
    def add(self):
        print("Adding...")       
        result = sum(self.operands_list) #effettua la somma di tutti gli elementi della lista
        self.save_operation('add',self.operands_list, result)
        
                            
    def sub(self):
                        
        result = reduce(lambda x,y: x-y, self.operands_list ) #sottrae tutti i termini della lsita
        self.save_operation('sub',self.operands_list, result)
                            
                            
    def mul(self):
        result = reduce(lambda x,y: x*y, self.operands_list ) #moltiplica tutti i termini della lista
        self.save_operation('mul',self.operands_list, result)  
            
    def div(self):
        try:
            result = reduce(lambda x,y: x/y, self.operands_list )
            self.save_operation('div',self.operands_list, result)
        except ZeroDivisionError:
            return -1
    
    def save_operation(self,operation,operands_list,result):
        
        self.dict_result ={
                "command": operation,
                "operands": operands_list,
                "Result": result   
            }
        
            
        

if __name__ == '__main__':
    conf = {
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    
    cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(Calculator(),'/',conf)