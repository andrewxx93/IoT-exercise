from operator import itemgetter
import json

if __name__ =='__main__':
    #list_to_be_sorted = [{'name':'Homer', 'age':'39'}, {'name':'Bart', 'age':'10'},{'name':'Bart', 'age':'30'}]
    with open("devices.json")as fp:
        catalog = json.load(fp)
    devices = catalog['devicesList']
    for device in devices:
        device["deviceID"] = int(device["deviceID"])
    
    devices = sorted(devices, key=lambda k: k['deviceID']) 
    for device in devices:
        device["deviceID"] = str(device["deviceID"])
    print(devices)