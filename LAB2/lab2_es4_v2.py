import cherrypy
import json
import os
from operator import itemgetter
class DeviceRegistering:
    exposed=True
    def __init__(self):
        self.__device_catalog_name = "devices.json"
        self.__device_catalog = {}
        
        
    def GET(self):
        return open('index.html','r')
    
    def POST(self):
        new_device = json.loads(cherrypy.request.body.read())
        if new_device["deviceID"] == "" or new_device["deviceName"] == "" or new_device["measureType"] =="" or new_device["unit"] == "":
            print("Insert all parameters")
        else:
            with open(self.__device_catalog_name,'r') as dc:
                self.__device_catalog = json.load(dc)
                
            devices = self.__device_catalog["devicesList"]
            IDcheck_precence = False
            for device in devices:
                if device["deviceID"] == new_device["deviceID"]:
                    IDcheck_precence = True
                
            if IDcheck_precence == False:
                self.__device_catalog["devicesList"].append(new_device)
                devices = self.__device_catalog['devicesList']
                
                ##################Sorting by device ID################
                # for device in devices:
                #     device["deviceID"] = int(device["deviceID"])
        
                # devices = sorted(devices, key=lambda k: k['deviceID']) 
                # for device in devices:
                #     device["deviceID"] = str(device["deviceID"])
                # print(devices)
                # self.__device_catalog["devicesList"] = devices
                ########################################################
                for device in self.__device_catalog["devicesList"]:
                    device["deviceID"] = int(device["deviceID"])
        
                self.__device_catalog["devicesList"] = sorted(self.__device_catalog["devicesList"], key=lambda k: k['deviceID']) 
                for device in self.__device_catalog["devicesList"]:
                    device["deviceID"] = str(device["deviceID"])
                #print(self.__device_catalog["devicesList"])
                #self.__device_catalog["devicesList"] = devices
                with open(self.__device_catalog_name,"w") as dc:
                    json.dump(self.__device_catalog,dc,indent=4)
                return json.dumps(self.__device_catalog)
            else:
                print("Device ID already present.Please choose another ID")
            
        
if __name__ == '__main__':
    conf = {
        '/':{
            'request.dispatch' : cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.abspath(os.getcwd()),
        },
        '/css':{
            'tools.staticdir.on' :True,
            'tools.staticdir.dir': './css'
        },
        '/js':{
            'tools.staticdir.on':True,
            'tools.staticdir.dir': './js'
        },
        
    }
    
    
    cherrypy.tree.mount(DeviceRegistering(),'/',conf)
    cherrypy.engine.start()
    cherrypy.engine.block()