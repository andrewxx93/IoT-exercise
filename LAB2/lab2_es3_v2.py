import cherrypy
import json
from functools import reduce
@cherrypy.expose
class RESTCalculator3(object):
    def __init__(self):
        self.__base_response = {
            
            "operands": [],
            "operation": None,
            "result" : None
        }
    def PUT(self):
        payload = json.loads(cherrypy.request.body.read())
        if len(list(payload.keys())) == 2:
            command = payload["command"]
            if command in ["add","sub","mul","div"]:
                if command == "add":
                    print("adding...")
                    payload["result"] = sum(payload["operands"])
                elif command == "sub":
                    print("subtracting...")
                    payload["result"] = reduce(lambda x,y: x-y,payload["operands"])
                elif command == "mul":
                    print("multiplying...")
                    payload["result"] = reduce(lambda x,y: x*y,payload["operands"])
                elif command == "div":
                    print("dividining...")
                    try:    
                        payload["result"] = reduce(lambda x,y: x/y,payload["operands"])
                    except ZeroDivisionError:
                        raise cherrypy.HTTPError(400,"You are trying to divide by zero!!!")
                return json.dumps(payload,indent=4)        
            else:
                raise cherrypy.HTTPError(501,f"Command {payload['command']} is not supported!!!")
        else:
            raise cherrypy.HTTPError(400,"The body contains a wrong number of parameters!!!")


if __name__== '__main__':
    conf = {
        '/':{
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tool.session.on' : True
        }
    }
    
cherrypy.tree.mount(RESTCalculator3(),'/',conf)
cherrypy.engine.start()
cherrypy.engine.block()