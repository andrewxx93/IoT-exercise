import cherrypy
import json
import time
import requests

@cherrypy.expose
class RESTCatalogManager:
    
    def __init__(self):
        #self.__catalog_name = catalog_name
        self.__catalog = {}
        self.__index_list = [] #list to store the researching index
        #Reading the device catalog stored in "catalog.json"
        with open('catalog.json') as fp:
            self.__catalog = json.load(fp)
        self.__base_device = {
            "deviceID": None,
            "deviceName": "",
            "measureType": [],
            "availableServices": [],
            "servicesDetails": [ ],
            "lastUpdate": ""
            
        }
    def GET(self,*uri,**params):
        if len(uri)!= 0:
            operation =uri[0]
            if operation.lower() == "search":
                self.__index_list = []
                if len(params.keys())!= 0:
                    search_type = list(params.keys())[0]
                    search_value = list(params.values())[0]
                    if search_type == "id":
                        search_type = "deviceID"
                        search_value = int(search_value)
                    if search_type == "name":
                        search_type = "deviceName"
                    
                    for i in range(len(self.__catalog["devicesList"])):
                        if self.__catalog["devicesList"][i][search_type].lower() == search_value.lower():
                            self.__index_list.append(i)
                    print(search_type)
                    print(search_value)
                    print(self.__index_list)
            elif operation.lower() == "insert":
                pass
            elif operation.lower() == "print":
                pass
            elif operation.lower() == "exit":
                pass
            else:
                raise cherrypy.HTTPError(501,"Operation not implemented")
        




if __name__ == '__main__':
    conf = {
        '/':{
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    }
    
    cherrypy.tree.mount(RESTCatalogManager(),'/',conf)
    cherrypy.engine.start()
    cherrypy.engine.block()