import json
from json.decoder import JSONDecodeError
import requests

if __name__ == '__main__':
    base_url = "http://localhost:8080/"
    message = "Insert the operation among 'add','sub','div','mul' followed by two operands. Press 'q' to quit:\n"
    while True:
        command = input(message).split(' ')
        if command[0] == 'q':
            break
        ###########FIRST WAY#############
        #complete_url = base_url+ command[0]+f"?op1={command[1]}&op2={command[2]}"
        #print(complete_url)
        #r = requests.get(complete_url)
        #################################
        
        ###########SECOND WAY#############
        if len(command) == 3:
            complete_url = base_url+ command[0]
            try: 
                r = requests.get(complete_url,params = {'op1':command[1],'op2':command[2]})
                r.raise_for_status()
                
            except (IndexError,ValueError):
                print("Check the number of operands!!!")
            except requests.exceptions.HTTPError as e:
                print("HTTP error")
                print(e)
            except json.simplejson.errors.JSONDecodeError:
                print("There was a problem with the json file...")
            except requests.exceptions.RequestException:
                print("Ops...something went wrong")
            else:
                print(f"You chose to perform the {command[0]} between {command[1]} and {command[2]}")
                print(f"The result is: {r.json()['result']}")
        else:
            print("Too operands!!!")       
                
            ##################################
        
        
    print("See you soon!")