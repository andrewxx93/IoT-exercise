def update_insert_device(self,dev_id):
        #time_update =  dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        #self.catalog_content["lastUpdate"] = time_update
        if (save_index :=self.searchByID(dev_id) ) == {}: #walrus operator !!!
            print("L'ID non è presente quindi devo aggiungere un nuovo dispositivo")
            new_name = input("Inserisci il nome: ")
            new_measure_type = input("Inserisci i tipi di misura separati da uno spazio: ").split(' ')
            new_measure_type = [item.upper() for item in new_measure_type]
            new_available_service = input("Inserisci i servizi disponibili separati da uno spazio: ")
            new_service_details_list = []
            
            for service in new_available_service:
                if service.upper() == 'REST':
                    print("Inserisci informazioni per il protocollo REST")
                    rest_ip = input("Inserisci l'indirizzo ip per REST: ")
                    d = {
                        "serviceType":service.upper(),
                        "serviceIP": rest_ip
                        }
                    new_service_details_list.append(d)
                elif service.upper() == 'MQTT':
                    print("Inserisci informazioni per il protocollo MQTT")
                    mqtt_ip = input("Inserisci l'indirizzo ip per MQTT: ")
                    topic_list = input("Inserisci la lista di topic da inserire separati da uno spazio").split(' ')
                    d = {
                        "serviceType":service.upper(),
                        "serviceIP": mqtt_ip,
                        "topic": topic_list
                        }
                    new_service_details_list.append(d)     
            new_device = {
                "deviceName" : new_name,
                "deviceID" : int(dev_id),
                "measureType": new_measure_type,
                "availableServices": new_available_service,
                "servicesDetails": new_service_details_list,
                "lastUpdate": time_update
                
            }
            self.catalog_content["devicesList"].append(new_device)
            
        else:
            print(f"Aggiorno il dispositivo con ID: {dev_id}")
            
            
            #variable initialization
            
            measure_type_list = []
            available_service_list = []
            device_name = ""
            service_details_list =[]
            self.catalog_content["devicesList"][save_index]["lastUpdate"] = time_update
            
            device_name = input("Insert the new device name: ")
            measure_type_list= input("Insert the available measure type separated by a space: ").split(' ')
            available_service_list = input("Insert the update available services separated by a space: ").split(' ')
            
            
            #Updating name
            if device_name !="":
                self.catalog_content["devicesList"][save_index]["deviceName"] = device_name.upper()
            
            
            #updating measure type list
            if measure_type_list != [""]:
                measure_type_list = [elem.upper() for elem in measure_type_list]
                self.catalog_content["devicesList"][save_index]["measureType"] = measure_type_list
                
                
            #updating available service list
            if available_service_list != [""]:
                available_service_list = [elem.upper() for elem in available_service_list]
                
            
                #updating service detail list
                for service in available_service_list:
                    if service == 'REST':
                        print("Inserisci informazioni per il protocollo REST")
                        rest_ip = input("Inserisci l'indirizzo ip per REST: ")
                        d = {
                            "serviceType":service.upper(),
                            "serviceIP": rest_ip
                            }
                        service_details_list.append(d)
                    elif service== 'MQTT':
                        print("Inserisci informazioni per il protocollo MQTT")
                        mqtt_ip = input("Inserisci l'indirizzo ip per MQTT: ")
                        topic_list = input("Inserisci la lista di topic da inserire separati da uno spazio").split(' ')
                        d = {
                            "serviceType":service.upper(),
                            "serviceIP": mqtt_ip,
                            "topic": topic_list
                            }
                        service_details_list.append(d) 
            
            self.catalog_content["devicesList"][save_index]["availableServices"] = available_service_list
            self.catalog_content["devicesList"][save_index]["servicesDetails"] = service_details_list
            
            
            
            #self.catalog_content["devicesList"][save_index]["measureType"].append(input("Inserisci il nuovo tipo di misura: "))
            #self.catalog_content["devicesList"][save_index]["availableServices"].append(input("Inserisci il nuovo servizio disponibile: "))
            print("--------------------------------------------")
            print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][save_index].items()))
            print("--------------------------------------------")