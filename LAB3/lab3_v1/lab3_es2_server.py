import os
import cherrypy
import json
from server_functions import DeviceManager

class WebDeviceManager():
    exposed=True
    
    def __init__(self):
        self.message = ""
        self.d = DeviceManager("catalog.json")
        self.change_check = 0   #this is a flag that tell us if some changes were mado to the catalog. 0--> NO CHANGE, 1--> CHANGE OCCOURRED        
    def GET(self, *uri, **params):

        #self.message = """Welcome to the devices catalog manager:<br>
        #    1)Search by name <br>
        #   2)Search by ID <br>
        #    3)Search by service<br>
        #    4)Search by measure type<br>
        #    5)Insert new device/update an existing one<br>
        #    6)Print all devices catalog<br>
        #   7)Quit (save the modified catalog)<br>
        #    8)Exit without saving<br>
        #   """
            
        if len(uri) == 0:
            return f'<h1 style="text-align:center">{self.message}</h1>'
        else:
            #The combination of next() + iter() returns the next element of the iterable: the iterable in this case
            #is the dict_keys element
            #It is more efficient then list() + dict.keys() list(params.keys())[0] since, if we have multiple keys, all the
            #keys are stored in the list and after that we extract the first element from the list
            if len(params) != 0:
                param_key = next(iter(params.keys()))
                param_value = params[param_key]
            result_list = []
            if uri[0] == "search":
                print("SEI NELLA SEZIONE SEARCH")
                if len(params) != 0:
                    
                    if param_key == 'name':
                        print("Searching by name...")
                        result_list = self.d.searchByName(param_value)
                        
                    elif param_key == 'id':
                        print("Searching by ID...")
                        result_list = self.d.searchByID(param_value)
                    
                    elif param_key == 'service':
                        print("Searching by Service...")
                        result_list = self.d.searchByService(param_value)
                        
                    elif param_key == 'measure':
                        print("Searching by measure...")
                        result_list = self.d.searchByMeasureType(param_value)
                        
                    
                    result_dict = {"result": result_list}   
                    return json.dumps(result_dict,indent=4)
            
                else:
                    raise cherrypy.HTTPError(400,"The query string cannot be empty")
                
            elif uri[0] == "checkID":
                
                print("SEI NELLA SEZIONE checkID")
                if param_key == 'id':
                    save_index = self.d.checkID(int(param_value)) #il valore di ritorno è l'indice dell'elemento trovato
                    if save_index == -1:  #There is no device with such an ID. 
                        print(f"The device with ID {param_value} is NOT present")
                        return json.dumps({"save_index": save_index, "insert_update": "insert"})
                        
                    else:
                        print(f"The device with ID {param_value} is already present")
                        return json.dumps({"save_index": save_index, "insert_update": "update"})
                        
                else:
                    raise cherrypy.HTTPError(400,"The request is not valid, please check the query string!!!")
                
            elif uri[0]=="print":
                print("Print the full catalog")
                self.d.print_catalog()
                return json.dumps(self.d.catalog_content)
            
            elif uri[0] == "exit":
                #saving routine on the file if some change was made. We need a flag to understand it.
                self.d.quit()
                print("Good by !!!")
                
            else:
                raise cherrypy.HTTPError(400,"The request path is invalid, please check the path string")
        
    def PUT(self,*uri,**params):
        #Since we want to add or update a device in the device catalog without changing the URL of this catalog, we
        #have to use the PUT method since it is idempotent so it cannot change the URL.
        #request_body = cherrypy.request.body.read()
        #print(json.loads(request_body))
        if len(uri)!=0:
            
            if uri[0] == 'insert':
                print("Calling insert method and adding the device")
                request_body_raw = cherrypy.request.body.read()      #Acquisisco il body della PUT request in raw
                new_device = json.loads(request_body_raw)            #converto il body in dict
                self.change_check = 1
                self.d.insert_device(new_device)
                
            elif uri[0] == 'update':
                print("Calling update method and update the device")
                request_body_raw = cherrypy.request.body.read()      #Acquisisco il body della PUT request in raw
                updated_device_information = json.loads(request_body_raw)            #converto il body in dict
                self.change_check = 1
                self.d.update_device(updated_device_information)
        else:
            raise cherrypy.HTTPError(400,"The request is not valid, please check")
    
if __name__ == '__main__':
    conf = {
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher()
        }
    }
    
    cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(WebDeviceManager(),'/',conf)

    

