import requests
import json

if __name__ == '__main__':
    message = """Insert the operation among:
    - addition        ---> add
    - subtraction     ---> sub
    - multiplication  ---> mul
    - division        ---> div
    - exit            ---> exit
    """
base_url = "http://299aaab8b7cc.ngrok.io/"
while True:
    print(message)
    command = input("Insert the desired command: ")
    if command.lower() in ["add", "sub", "mul", "div"]:
        op1 = input("Insert the first operand: ")
        op2 = input("Insert the second operand: ")
        complete_url = base_url + f"{command}?op1={op1}&op2={op2}"
        print(complete_url)
        r = requests.get(complete_url)
        print(type(r.content))
        result = r.json()
        print("The result is: " + str(result["Result"]))
        
    elif command.lower() == 'exit':
        exit()
    else:
        print("Unkonw command")
        break
        