import json
from operator import itemgetter
import requests


#Function used to get the json file from the remote server in Barcelona(Spain)
def get_json():
    print("Getting the JSON file from the server in Barcelona")
    try:
        r = requests.get("https://www.bicing.barcelona/en/get-stations")
        bike_sharing_json = r.json()
                
        #r.raise_for_status is only for debug and testing 
        #print(r.raise_for_status())
            
    except requests.exceptions.HTTPError as errh:
        print ("Http Error:",errh)
    except requests.exceptions.ConnectionError as errc:
        print ("Error Connecting:",errc)
    except requests.exceptions.Timeout as errt:
        print ("Timeout Error:",errt)
    except requests.exceptions.RequestException as err:
        print ("OOps: Something Else",err)

    return bike_sharing_json




#Points 1 and 2 of the exercise:
#1) Sort by available slots 
#2) Sort by available bikes

#The mandatory bike sharing list argument is the list of all the stations retrieved from the json file
#The mandatory sort key is the key used by the function to sort the stations
#The optional argument N indicates the number of stations to display
#The optional argument sort_type indicates the so type: ascending or descending

def sort_stations(bike_sharing_list, sort_key, N, sort_type):
    s = True #Default value of sorted function. It sorts in descendig way
    
    if sort_type == "ascending":
        s = False   #Sorted will sort in ascending way
    
    #The function sorted returns a sorted list of the specified iterable object (in this case a list of dict).
    #The key params is a function to be executed to decide how the element must be sorted(In this case it is used the itemgetter method)
    #The operator.itemgetter method return a callable object that fetches items from operator (in this case bike_sharing_list)
    #and uses the value of sort_key to sort the list of dict by the valus of the key sort_key. In this case sort_key could be a string
    # slots or bikes
    sorted_list = sorted(bike_sharing_list, key = itemgetter(sort_key), reverse = s)  
    if  N > len(sorted_list):
        N = len(sorted_list)
    
    #With sliding we can decide how many elements must be returned
    return sorted_list[:N]
    
    
#Point 3 of the exercise.
#The function returns a list of stations with more then N electrical bikes and more then M available slots.

#The mandatory bike sharing list argument is the list of all the stations retrieved from the json file
#The optional argument N indicates the lower bound of the number of electrical bikes we are looking for
#The optional argument M indicates the lower bound of the number of free slots we are looking for
    
def customsearch(bike_stations_list,N,M):
    customsearch_result_list = []
    
    bike_stations_list_iterator = iter(bike_stations_list)
    
    for station_elem in bike_stations_list_iterator:
        if "electrical_bikes" in station_elem.keys():
            if station_elem["electrical_bikes"] >= N and station_elem["slots"] >= M:
                customsearch_result_list.append(station_elem)
                
    return customsearch_result_list



def counting(bike_stations_list):
    
    total_free_slots = 0
    total_available_bikes = 0
    for i in range(len(bike_stations_list)):
        total_free_slots += bike_stations_list[i]["slots"]
        total_available_bikes += bike_stations_list[i]["bikes"]
        
    return total_free_slots, total_available_bikes    
    
    
    

def pretty_print(list_of_dict_to_print):
    for i in range(len(list_of_dict_to_print)):
        print("------------------------------")
        print("\n".join("{}\t{}".format(k,v) for k,v in list_of_dict_to_print[i].items()))
        print("------------------------------")
        
        
        
def print_one_key(list_of_dict_to_print, key):
    for i in range(len(list_of_dict_to_print)):
        
        print("{}\t{}".format(key, list_of_dict_to_print[i][key]))
        