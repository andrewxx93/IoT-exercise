import json
import cherrypy
import os
class NiceCalculator(object):
    exposed=True
    
    def __init__(self):
            self.operation = ""
            self.operand_list = []
            self.dict_result ={}
            self.result = None
            
    def GET(self,*uri,**params):
        
        if len(params)==0:
            try:
                return open("index.html",'r')
            except:
                print("Error")
                
        elif len(params)!=0:
            self.operation = params["command"]
            print("The selectedoperation is " + params["command"])               
            if self.operation == "quit":
                print("See you next time")
                exit()
            else:
                if self.operation in ["add","sub","mul","div"]:
                    
                    if len(params) == 3:
                        self.operand_list.clear()
                        self.operand_list.append(float(params["op1"]))
                        self.operand_list.append(float(params["op2"]))
                        
                        
                        if self.operation == "add":
                            self.result = self.add(self.operand_list[0],self.operand_list[1])
                            
                        elif self.operation == "sub":
                            self.result = self.sub(self.operand_list[0],self.operand_list[1])
                            
                        elif self.operation == "mul":
                            self.result = self.mul(self.operand_list[0],self.operand_list[1])
                            
                        elif self.operation == "div":
                            self.result = self.div(self.operand_list[0],self.operand_list[1])
                        
                        
                            
                    else:
                        print("--------------------------------")
                        print("Wrong operand number. The operand must be only 2")
                        print("--------------------------------")
                        return '<h1 style= "text-align:center">Wrong operand number. The operand must be only 2</h1>'
                else:
                    print("--------------------------------")
                    print("Unknown operation")
                    print("--------------------------------")
                    return '<h1 style= "text-align:center">Unknown operation</h1>'
        else:
            print("--------------------------------")
            print("Input string cannot be empty")
            print("--------------------------------")
            return '<h1 style= "text-align:center">Input string cannot be empty</h1>'

        if self.result != None:
            self.print_result(self.result)  
            self.save_operation()
            if self.result != 'Infinity':
                #return json.dumps(self.dict_result,indent = 4)
                return f"<h1 style= 'text-align:center '>The result is: {self.result}</h1>"
            else:
                return "<h1 style= 'text-align:center '> Ops...you are trying to divide by zero</h1>"

        
            
            
    def add(self,a,b):
        print("Summing...")
        return a+b
        
    def sub(self,a,b):
        print("Subtracting...")
        return a-b
    
    def mul(self,a,b):
        print("Multiplying...")
        return a*b
    
    def div(self,a,b):
        print("Dividing...")
        try:
            return a/b
        except ZeroDivisionError:
            print("Ops...you are trying to divide by zero")
            return "Infinity"
        
        
    def print_result(self,result):
            print(result)
    
    def save_operation(self,):
        self.dict_result ={
                "Operation": self.operation,
                "First operand": self.operand_list[0],
                "Second operand": self.operand_list[1],
                "Result": self.result
                
            }
        
        with open("operation.json",'w') as fp:
            json.dump(self.dict_result,fp,indent=4)
            
        

if __name__ == '__main__':
        
    conf ={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tools.staticdir.root':os.path.abspath(os.getcwd()),   #setto la directory corrente come root
        },
        '/css':{
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './css'
        },
        '/js':{
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './js'
        },
    }
        
    cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(NiceCalculator(),'/',conf)