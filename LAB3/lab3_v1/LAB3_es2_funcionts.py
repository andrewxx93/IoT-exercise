inserimento dati per nuovo dispositivo

"""
                    #Acquisire tutte le informazioni relative al nuovo device da inserire nel catalogo
                    #passare il dizionario del nuovo dispositivo come stringa in data e inviarlo con requests.put
                    #Il server chiama il metodo insert che deve  aggiungere l'ID e il timestamp 
                    name = input("Inserisci il nome: ")
                    
                    measure_type = input("Inserisci i tipi di misura separati da uno spazio: ").split(' ')
                    measure_type = [item.upper() for item in measure_type]
                    
                    available_service = input("Inserisci i servizi disponibili separati da uno spazio: ").split(' ')
                    available_service = [item.upper() for item in available_service]
                    
                    
                    service_details_list = []
                    for service in available_service:
                        if service == 'REST':
                            print("Inserisci informazioni per il protocollo REST")
                            rest_ip = input("Inserisci l'indirizzo ip per REST: ")
                            d = {
                                "serviceType":service,
                                "serviceIP": rest_ip
                                }
                            service_details_list.append(d)
                        elif service == 'MQTT':
                            print("Inserisci informazioni per il protocollo MQTT")
                            mqtt_ip = input("Inserisci l'indirizzo ip per MQTT: ")
                            topic_list = input("Inserisci la lista di topic da inserire separati da uno spazio: ").split(' ')
                            d = {
                                "serviceType":service,
                                "serviceIP": mqtt_ip,
                                "topic": topic_list
                                }
                            service_details_list.append(d)
                            
                    new_device = {
                        "deviceName" : name,
                        "deviceID" : int(dev_id),
                        "measureType": measure_type,
                        "availableServices": available_service,
                        "servicesDetails": service_details_list,
                        "lastUpdate": ""
                    }
                    
                    r = requests.put(base_url + "insert", json = new_device)
                    print(r.status_code)
                """   





Inserimento dati per aggiornare dispositivo già presente



"""
                    #variable initialisation
                    new_name = ""
                    new_measure_type = []
                    new_available_service = []
                    new_service_details=[]
                    
                    new_name = input("Insert the new device name: ").upper()
                    print(new_name)
                    
                    new_measure_type= input("Insert the available measure type separated by a space: ").split(' ')
                    new_measure_type = [item.upper() for item in new_measure_type]
                    print(new_measure_type)
                    
                    new_available_service= input("Insert the update available services separated by a space: ").split(' ')
                    new_available_service= [item.upper() for item in new_available_service]
                    print(new_available_service)
                    
                    
                    for service in new_available_service:
                        if service == 'REST':
                            print("Inserisci informazioni per il protocollo REST")
                            rest_ip = input("Inserisci l'indirizzo ip per REST: ")
                            d = {
                                "serviceType":service,
                                "serviceIP": rest_ip
                                }
                            new_service_details.append(d)
                            
                        elif service == 'MQTT':
                            print("Inserisci informazioni per il protocollo MQTT")
                            mqtt_ip = input("Inserisci l'indirizzo ip per MQTT: ")
                            topic_list = input("Inserisci la lista di topic da inserire separati da uno spazio: ").split(' ')
                            d = {
                                "serviceType":service,
                                "serviceIP": mqtt_ip,
                                "topic": topic_list
                                }
                            new_service_details.append(d)
                    
                    updated_device_information = {
                        "save_index" : check_id_get_request["save_index"],
                        "deviceID" : int(dev_id),
                        "deviceName" : new_name,
                        "measureType": new_measure_type,
                        "availableServices": new_available_service,
                        "servicesDetails": new_service_details,
                        "lastUpdate": ""
                    }
                    """


