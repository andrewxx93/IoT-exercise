import requests
import json
from lab3_es2_client_functions import *

if __name__ == '__main__':
    message = """
----------------------------------------------------
Welcome to the Device Manager Client.
Choose the desired operation you wanna do among:

1)Search by name

2)Search by ID

3)Search by service

4)Search by measure type

5)Insert/update device

6)Print all catalog

7)Quit
----------------------------------------------------
    """
    while True:
        operation = ""
        base_url = "http://127.0.0.1:8090/"
        print(message)
        
        print("Insert IP and port of the server. If blank the defaul value is http://127.0.0.1:8090/")
        IP = input("Please enter the ip address of the server as '127.0.0.1:8090/':\n")
        port = input("Please enter the port of the server:\n")
        
        if IP !="" and port !="":
            base_url = "http://" + IP + ":" + port + "/"
        
        """
        while base_url=='':
            print("The URL cannot be empty") 
            base_url = input("Please enter the base URL of the catalog as 'http://127.0.0.1:8090/':\n")
        """
        
        operation = int(input("Insert the desired operation: "))
        
        if operation == 1:
            #Search by name
            print("Searching by name")
            name = input("Enter the name of the device: ")
            
            url = base_url +"search" + "?" + f"name={name}"
            print(url)
            
            try:
                r = requests.get(url)
                r.raise_for_status()
            except requests.exceptions.HTTPError as errh:
                print ("Http Error:",errh)
            except requests.exceptions.ConnectionError as errc:
                print ("Error Connecting:",errc)
            except requests.exceptions.Timeout as errt:
                print ("Timeout Error:",errt)
            except requests.exceptions.RequestException as err:
                print ("OOps: Something Else",err)
            
            
            get_response_content = r.json()

            device_list = get_response_content["result"]
            
            #Debug code
            #print(device_list)
            #print(len(device_list))
            
            #Stampa in maniera ordinata il risultato dell'operazione di ricerca
            print_device(device_list)
            
        elif operation == 2:
            #Search by ID
            print("Searching by ID")
            id = input("Enter the ID of the device: ")
            
            url = base_url +"search" + "?" + f"id={id}"
            print(url)
            
            try:
                r = requests.get(url)
                r.raise_for_status()
            except requests.exceptions.HTTPError as errh:
                print ("Http Error:",errh)
            except requests.exceptions.ConnectionError as errc:
                print ("Error Connecting:",errc)
            except requests.exceptions.Timeout as errt:
                print ("Timeout Error:",errt)
            except requests.exceptions.RequestException as err:
                print ("OOps: Something Else",err)
            
            
            get_response_content = r.json()

            device_list = get_response_content["result"]
            
            #Debug code
            #print(device_list)
            #print(len(device_list))
            
            #Stampa in maniera ordinata il risultato dell'operazione di ricerca
            print_device(device_list)
            
        elif operation == 3:
            #Search by service
            print("Searching by service")
            service = input("Enter the service of the device: ")
            
            url = base_url +"search" + "?" + f"service={service}"
            print(url)
            
            try:
                r = requests.get(url)
                r.raise_for_status()
            except requests.exceptions.HTTPError as errh:
                print ("Http Error:",errh)
            except requests.exceptions.ConnectionError as errc:
                print ("Error Connecting:",errc)
            except requests.exceptions.Timeout as errt:
                print ("Timeout Error:",errt)
            except requests.exceptions.RequestException as err:
                print ("OOps: Something Else",err)
            
            
            get_response_content = r.json()

            device_list = get_response_content["result"]
            
            #Debug code
            #print(device_list)
            #print(len(device_list))
            
            #Stampa in maniera ordinata il risultato dell'operazione di ricerca
            print_device(device_list)
        
        elif operation == 4:
            #Search by measure
            print("Searching by measure")
            measure = input("Enter the measure of the device: ")
            
            url = base_url +"search" + "?" + f"measure={measure}"
            print(url)
            
            try:
                r = requests.get(url)
                r.raise_for_status()
            except requests.exceptions.HTTPError as errh:
                print ("Http Error:",errh)
            except requests.exceptions.ConnectionError as errc:
                print ("Error Connecting:",errc)
            except requests.exceptions.Timeout as errt:
                print ("Timeout Error:",errt)
            except requests.exceptions.RequestException as err:
                print ("OOps: Something Else",err)
            
            
            get_response_content = r.json()

            device_list = get_response_content["result"]
            
            #Debug code
            #print(device_list)
            #print(len(device_list))
            
            #Stampa in maniera ordinata il risultato dell'operazione di ricerca
            print_device(device_list)
        
        elif operation == 5:
            dev_id = input("Insert the device ID: ")
        
            query_string = {'id': str(dev_id)}
            try:
                r = requests.get(base_url +"checkID",params = query_string, timeout = 10)
                r.raise_for_status()
                
                check_id_get_request = json.loads(r.content)
                decision = check_id_get_request["insert_update"]    #se insert_update = 1---> aggiungere nuovo dispositivo
                                                                    #se insert_update = 2 ---> aggiornare dispositivo       
                if decision == "insert":
                    
                    command = input("ID not present yet. Do you want to insert a new device ?\n\nType Y/N: ").upper()
                    if command == "Y":
                        print("Add all the information for the new device...")
                        new_device = insert_new_device_information(dev_id)
                        r = requests.put(base_url + "insert", headers = {'Content-Type': 'text/plain'},json = new_device)
                        print(r.status_code)
                        
                    else:
                        print("Choose another operation")
                
                elif decision == "update":
                    command = input("ID already exists.Do you want to update the device ?\n\nType Y/N: ").upper()
                    
                    #Inserire i parametri che si desiderano aggiornare e lasciare vuoti quelli che non si desidera cambiare
                    #solo l'ID deve rimanere lo stesso.
                    #Il client passa solo i campi,anche vuoti, con un file JSON. Il server chiama il metodo update che andrà
                    #ad aggiornare solo i campi non vuoti passati nel file JSON. Il metodo GET ritorna sia il comando insert/update sia il "save index".
                    #Nel caso in cui l'ID è gia presente save index = -1, altrimenti è l'indice del catalogo memorizzato localmente.
                    #Prima di aggiornare i dati controllare che l'ID sia quello corretto altrimenti stampare messaggio di errore.
                    
                    if command == "Y":
                        
                        print("Adding the updating information for the device...")
                        
                        print(f"Aggiorno il dispositivo con ID: {dev_id}")
                        
                        updated_device_information = insert_update_device_information(dev_id,check_id_get_request)
                        
                        r = requests.put(base_url + "update", headers = {'Content-Type': 'text/plain'},json = updated_device_information)
                        print(r.status_code)
                        
                        
                        #Creazione file JSON da passare
                        

                    else:
                        print("Choose another operation")
                
                
            except requests.exceptions.HTTPError as errh:
                print ("Http Error:",errh)
            except requests.exceptions.ConnectionError as errc:
                print ("Error Connecting:",errc)
            except requests.exceptions.Timeout as errt:
                print ("Timeout Error:",errt)
            except requests.exceptions.RequestException as err:
                print ("OOps: Something Else",err)
        
        elif operation == 6:
            #Print alla catalog
            url = base_url + "print"
            print(url)
            
            try:
                r = requests.get(url)
                r.raise_for_status()
            except requests.exceptions.HTTPError as errh:
                print ("Http Error:",errh)
            except requests.exceptions.ConnectionError as errc:
                print ("Error Connecting:",errc)
            except requests.exceptions.Timeout as errt:
                print ("Timeout Error:",errt)
            except requests.exceptions.RequestException as err:
                print ("OOps: Something Else",err)
            
            
            
            #Stampa in maniera ordinata tutto il catalogo
            get_response_content = r.json()
            print_catalog(get_response_content)
        
        elif operation == 7:
            #Print alla catalog
            url = base_url + "exit"
            print(url)
            
            try:
                r = requests.get(url)
                r.raise_for_status()
            except requests.exceptions.HTTPError as errh:
                print ("Http Error:",errh)
            except requests.exceptions.ConnectionError as errc:
                print ("Error Connecting:",errc)
            except requests.exceptions.Timeout as errt:
                print ("Timeout Error:",errt)
            except requests.exceptions.RequestException as err:
                print ("OOps: Something Else",err)
        
        else:
            print("Unknown operation")
        
        
        
        
        
        
        
        