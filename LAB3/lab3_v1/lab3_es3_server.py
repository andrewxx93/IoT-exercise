import cherrypy
import json
from lab3_es3_server_functions import *

class BikeSharingInfo(object):
    exposed=True
        
    def GET(self,*uri,**query):
        
        if len(uri) != 0:
            
            #Default value of parameters
            N = 10 #Default value for number of stations to display or min value for electrical_bikes
            M = 5 #Defaul value for the min value for the free slots
            sort_type = "discendig" #Default type of sorting  
            
            
            if (query_len:=len(query)) != 0:
                
                for key in query.keys():
                    print("{}:{}".format(key,query[key]))
                    if key.upper() == 'N':
                        N = int(query[key])
                    if key.upper() == 'ASCENDING':
                        sort_type = "ascending"
                    if key.upper() == 'M':
                        M = int(query[key])             
            
                
                
            #sorting bike-stations by available slots
            if uri[0] == "sortbyslots":
                bike_sharing_json = get_json()
                
                sort_key = "slots"
            
                results = sort_stations(bike_sharing_json["stations"], sort_key, N, sort_type)
                
                
                pretty_print(results) 
                
                
                #print(len(result_list))
                
            #sorting bike-stations by available bikes
            elif uri[0] == "sortbybikes":
                bike_sharing_json = get_json()
                sort_key = "bikes"
            
                results = sort_stations(bike_sharing_json["stations"],sort_key, N, sort_type)  
                pretty_print(results)
                #print_one_key(result_list,"bikes")
                
            #get bike-stations with n° of electric bike > N and free slots > M
            elif uri[0] == "customsearch":
                bike_sharing_json = get_json()
                print("Call customsearch")
                results = customsearch(bike_sharing_json["stations"],N,M)
                print(len(results))
            
            elif uri[0] == "counting":
                bike_sharing_json = get_json()
                print("Counting in progress...")
                total_free_slots,total_available_bikes = counting(bike_sharing_json["stations"])
                
                results = {
                                "total free slots":total_free_slots,
                                "total available bikes":total_available_bikes
                                
                                }
            
            else:
                return "<h1 style = 'text-align: center'> Ops....the uri is wrong</h1>"
                
            result_json = {
            f"result of {uri[0]}" : results
            }   
            
            return json.dumps(result_json,indent = 4)
            
        else:
                
            return "<h1 style = 'text-align: center'> Ops....the uri is empty</h1>"
                
        
            
            
        #return json.dumps(bike_sharing_json)


if __name__ == '__main__':
    
    conf = {
        '/':
            {
                "request.dispatch": cherrypy.dispatch.MethodDispatcher()
                
            }
        
    }
    
    cherrypy.config.update({'server.socket_port':8080})
    cherrypy.quickstart(BikeSharingInfo(),'/',conf)