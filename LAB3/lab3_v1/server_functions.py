import json
import datetime as dt

class DeviceManager(object):
    def __init__(self,catalog_name):
        self.catalog_name = catalog_name
        self.result_list = []
        with open(self.catalog_name) as fp:
            self.catalog_content = json.load(fp)
            
    def print_device_info(self,device):
        pass

    def searchByName(self,dev_name):
        self.result_list.clear()
        #La funzione prende in ingresso il nome di un dispositivo e lo cerca dentro la devicesList
        #Prima di cercarlo le parole sono trasformate in maiuscolo in modo da rendere l'input CASE INSENSITIVE
        #Per stampare il risultato in maniera ordinata uso il metodo join() che ritorna una stringa composta
        #da un elemento iterabile (come le liste) e una stringa separatore. In questo caso il separatore è "\n"
        for i in range(len(self.catalog_content["devicesList"])):
            if self.catalog_content["devicesList"][i]["deviceName"].upper() == dev_name.upper():
                print("--------------------------------------------")
                print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
                print("--------------------------------------------")
                self.result_list.append(self.catalog_content["devicesList"][i])
                
        if self.result_list == []:
            return []
        else:
            return self.result_list
                
    def searchByID(self,dev_id):
        self.result_list.clear()
        save_index = -1
        for i in range(len(self.catalog_content["devicesList"])):
            if self.catalog_content["devicesList"][i]["deviceID"]== int(dev_id):
                print("--------------------------------------------")
                print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
                print("--------------------------------------------")
                save_index = i
                
        if save_index == -1:
            print(f"Il dispositivo con ID: {dev_id} non esiste")
            return []
        else:
            self.result_list.append(self.catalog_content["devicesList"][save_index])
            return self.result_list
        
    def checkID(self,dev_id):
        save_index = -1
        
        for i in range(len(self.catalog_content["devicesList"])):
            if self.catalog_content["devicesList"][i]["deviceID"] == int(dev_id):
                print("--------------------------------------------")
                print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
                print("--------------------------------------------")
                save_index = i
                
        return save_index

    def insert_device(self,new_device):
        time_update =  dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print("Insert new device in the catalog")
        
        
        self.catalog_content["lastUpdate"] = time_update   #aggiorno la data di ultimo aggiornamento/modifica al catalogo
        new_device["lastUpdate"] = time_update             #aggiorno la data di ultima modifica al device
        
        self.catalog_content["devicesList"].append(new_device)
        
        
        #Testing code
        for i in range(len(self.catalog_content["devicesList"])):
            print("--------------------------------------------")
            print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
            print("--------------------------------------------")
        
    
    def update_device(self,updated_device_information):
        time_update =  dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        save_index = updated_device_information["save_index"]
        print("Updating the device in the catalog with the new values")
        print("--------------------------------------------")
        print(updated_device_information)
        
        #Controllo ID dispositivo da aggiornare con l'ID corrispondente al device nel catalog con indice save_index
        if updated_device_information["deviceID"] == self.catalog_content["devicesList"][save_index]["deviceID"]:
            print("The ID is correct. Updating device information")
            self.catalog_content["lastUpdate"] = time_update                                        #aggiorno la data di ultimo aggiornamento/modifica al catalogo
            self.catalog_content["devicesList"][save_index]["lastUpdate"] = time_update             #aggiorno la data di ultima modica del device

        
            #Updating name
            if (device_name:=updated_device_information["deviceName"])!="":
                self.catalog_content["devicesList"][save_index]["deviceName"] = device_name
            #updating measure type
            if (measure_type:=updated_device_information["measureType"])!=[""]:
                self.catalog_content["devicesList"][save_index]["deviceName"] = measure_type
                
            #Updating available services and services details
            if (available_service:=updated_device_information["availableServices"])!=[""]:
                self.catalog_content["devicesList"][save_index]["availableServices"] = available_service
                self.catalog_content["devicesList"][save_index]["servicesDetails"] = updated_device_information["servicesDetails"]
            
            self.print_catalog()
                
                
        else:
            print("Wrong ID !!!!")
    
    def searchByService(self,service):
        self.result_list.clear()
        
        for i in range(len(self.catalog_content["devicesList"])):
            if service.upper() in self.catalog_content["devicesList"][i]["availableServices"]:
                print("--------------------------------------------")
                print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
                print("--------------------------------------------")
                self.result_list.append(self.catalog_content["devicesList"][i])
                
            if self.result_list == []:
                print(f"No devices were found with requested service: {service}")
                return []
            else:
                return self.result_list
                
        return self.result_list   
    
    def searchByMeasureType(self,measure):
        self.result_list.clear()
        for i in range(len(self.catalog_content["devicesList"])):
            if measure.upper() in self.catalog_content["devicesList"][i]["measureType"]:
                print("--------------------------------------------")
                print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
                print("--------------------------------------------")
                self.result_list.append(self.catalog_content["devicesList"][i])
                
        if self.result_list == []:
            print(f"No devices were found with requested measure type: {measure}")
            return []
        else:
            return self.result_list
            
    
            
        
    def print_catalog(self):
        print('-------------------------------------\n'
            f'owner: {self.catalog_content["projectOwner"]}\n'
            f'project name: {self.catalog_content["projectName"]}\n'
            f'last update: {self.catalog_content["lastUpdate"]}\n'
            '-------------------------------------\n'
            )
        for i in range(len(self.catalog_content["devicesList"])):
            print("--------------------------------------------")
            print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
            print("--------------------------------------------")
            
    def quit(self):
        print('-------------------------------------\n'
            f'owner: {self.catalog_content["projectOwner"]}\n'
            f'project name: {self.catalog_content["projectName"]}\n'
            f'last update: {self.catalog_content["lastUpdate"]}\n'
            '-------------------------------------\n'
            )
        for i in range(len(self.catalog_content["devicesList"])):
            print("--------------------------------------------")
            print("\n".join("{}:\t{}\n".format(k,v)for k,v in self.catalog_content["devicesList"][i].items()))
            print("--------------------------------------------")
        
        print(type(self.catalog_content))
        
        with open(self.catalog_name,'w') as fp:
            json.dump(self.catalog_content,fp,indent = 4)
            
