import paho.mqtt.client as PahoMQTT


class MyPublisher(object):
    def __init__(self,clientID, broker, port,set_status):
        self.clientID = clientID
        
        #Create an instace of paho.mqtt.client
        self._paho_mqtt = PahoMQTT.Client(clientID,True)
        
        #register the callback
        self._paho_mqtt_on_connect = self.myOnConnect
        
        self.messageBroker = broker
        self.port = port
        
    def start(self):
        #manage the connession to the broker
        
        self._paho_mqtt.connect(self.messageBroker, self.port)
        self._paho_mqtt.loop_start()
        
    def stop(self):
        self._paho_mqtt.loop_stop()
        self._paho_mqtt.disconnect()
        
    def myOnConnect(self, paho_mqtt, userdata, flags, rc):
        print("Connected to %s with result code: %d" %(self.messageBroker, rc))
        
    def myPublish(self,topic, message, QoS):
        self._paho_mqtt.publish(topic, message, QoS)
        

class MySubscriber(object):
    def __init__(self,clientID, topic, broker, port,set_status):
        self.clientID = clientID
        
        #Create an instace of paho.mqtt.client
        self._paho_mqtt = PahoMQTT.Client(clientID,True)
        
        #register the callback
        self._paho_mqtt.on_connect = self.myOnConnect
        self._paho_mqtt.on_message = self.myOnMessageReceived
        self.topic = topic        
        self.messageBroker = broker
        self.port = port
        self.set_status = set_status
        
    def start(self):
        #manage the connession to the broker
        print("starting connection")
        self._paho_mqtt.connect(self.messageBroker)
        print("connected")
        self._paho_mqtt.loop_start()
        self._paho_mqtt.subscribe(self.topic,2)
        
    def stop(self):
        print("Stopping")
        self._paho_mqtt.unsubscribe(self.topic)
        self._paho_mqtt.loop_stop()
        self._paho_mqtt.disconnect()
        print("stopped")
        
    def myOnConnect(self, paho_mqtt, userdata, flags, rc):
        print("Connected to %s with result code: %d" %(self.messageBroker, rc))
        
    def myOnMessageReceived(self, paho_mqtt, userdata, msg):
        #print("Topic:'" + msg.topic + "', QoS:'" + str(msg.qos) + "' Message:'" + str(msg.payload) + "'")
        self.set_status.setStatus(msg.topic,msg.payload)
        