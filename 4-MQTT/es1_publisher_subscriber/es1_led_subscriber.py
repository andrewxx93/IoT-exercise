from MyMqtt_publisher_subscriber import *
import json
import time

class Led():
    def __init__(self,clientID, topic, broker, port):
        self.client = MySubscriber(clientID, topic, broker, port,self)
        self.topic = topic
        self.status = None
        
    def start(self):
        self.client.start()
    
    def stop(self):
        self.client.stop()
        
    def setStatus(self,topic, msg):
        payload = json.loads(msg)
        self.status = payload["value"]
        pubID = payload["clientID"]
        timestamp = payload["timestamp"]
        print(f"The led has been set to {self.status} at {timestamp} by {pubID}\n")
    
if __name__ == '__main__':
    conf = json.load(open("settings.json"))
    broker = conf["broker"]
    port = conf["port"]    
    
    myLed = Led("myLed","IoT/Andrea/led", broker, port)
    
    myLed.start()
    while True:
        time.sleep(3)
    
    
    myLed.stop()