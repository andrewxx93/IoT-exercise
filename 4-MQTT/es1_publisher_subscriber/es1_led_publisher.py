from MyMqtt_publisher_subscriber import *
import json
import time

class LedManager(object):
    def __init__(self,clientID, topic, broker, port):
        self.topic = topic
        self.__message = {'clientID':clientID, 'n':'led','value': None,'timestamp':'','unit':'bool'}
        self.client = MyPublisher(clientID, broker, port,None)
        
    def start(self):
        self.client.start()

        
    def stop(self):
        self.client.stop()
    
    def publish(self,value):
        message = self.__message
        message['value'] = value
        message['timestamp'] =str(time.time())
        self.client.myPublish(self.topic,json.dumps(message),2)
        print("published")
        
    
    
    
if __name__ == '__main__':
    conf = json.load(open("settings.json"))
    broker = conf["broker"]
    port = conf["port"]    
    
    myLedManager = LedManager("myLed12312312","IoT/Andrea/led", broker, port)
    
    myLedManager.start()
    
    done = False
    print("Write Off to turn off the LED, Write On to turn on the LED, write q to exit\n")
    while not done:
        user_command = input("Write your choice: ")
        if user_command == "off" or user_command == "on":
            myLedManager.publish(user_command)
        elif user_command == "q":
            myLedManager.stop()
            done = True
        else:
            print("Command not valid")
            
    print("Goodbyeee!!!!")
    