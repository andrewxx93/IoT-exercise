from generalClient import *
import json
import random 
import time
class SensorClient(object):
    def __init__(self,clientID,broker,port,baseTopic):
        self.clientID = clientID
        self.client = MyMQTT(clientID,broker,port,self)
        self.baseTopic = baseTopic
        
    def start(self):
        print(f"starting the client {self.clientID}")
        self.client.start()
    
    def stop(self):
        print(f"stopping the client {self.client}")
        self.client.stop()
        
    def subscribe(self,topic):
        print(f"Subscribing to topic {topic}")
        self.client.mySubscriber(topic)
    
    def notify(self,topic,payload):
        payload = json.loads(payload)
        print(json.dumps(payload,indent = 4))
    

if __name__ == "__main__":
    with open("settings.json") as fp:
        conf = json.load(fp)
    
    clientID = "AndreaCatania"+ str(random.randint(1, 10**3))
    broker = conf["broker"]
    port = conf["port"]
    baseTopic = conf["baseTopic"]
    
    sensor_client= SensorClient(clientID, broker, port,baseTopic)
    sensor_client.start()
    time.sleep(0.5)
    print(f"Starting the client {clientID} that follows the data coming from base topic: {sensor_client.baseTopic}")
    
    
    command = ""
    

    while command != 'q':
        print("Select the operation you wanna do\n")
        print("a) Obtain data from all sensors in the building")
        print("f) Obtain data from all sensors in a specific floor")
        print("r) Obtain data from all sensors in a specific room")
        print("c) Return to this menu")
        print("q) Exit from the client")
        command = input("Write your choice: ")
        if command == 'q':
            print("Goodbye")
            break
        while command != 'c':
            if command == 'a':
                print("Data from all the sensors in the building")
                sensor_client.client.unsubscribe()
                sensor_client.subscribe(sensor_client.baseTopic +"/#")
                
            elif command == 'f':
                print("Data from all the sensors in selected floor")
                sensor_client.client.unsubscribe()
                floor = str(input("Select floor from 0->4: "))
                sensor_client.subscribe(sensor_client.baseTopic +"/"+ floor+"/#")
            elif command == 'r':
                print("Data from all the sensors in selected room")
                sensor_client.client.unsubscribe()
                floor = str(input("Select floor from 0->4: "))
                room = str(input("Select room from 0->3: "))
                sensor_client.subscribe(sensor_client.baseTopic +"/"+ floor +"/"+ room +"/#")
            else:
                print("Command not valid!!!")
            command = input()
        sensor_client.client.unsubscribe()
        
    
    sensor_client.stop()