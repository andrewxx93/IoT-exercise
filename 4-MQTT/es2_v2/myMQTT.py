import paho.mqtt.client as PahoMQTT
import json

class MyMQTT:
    def __init__(self,clientID,broker,port,notifier = None):
        self.__clientID = clientID
        self.__brokerURL = broker
        self.__brokerPort = port
        self.notifier = notifier
        
        #MQTT client creation
        self.__mqtt_client = PahoMQTT.Client(self.__clientID,False)
        
        self.__topic = ""
        self.__isSubscriber = False
        
        #Callbacks registration
        self.__mqtt_client.on_connect = self.onMyConnect
        self.__mqtt_client.on_message = self.onMyMessage
        self.__mqtt_client.on_publish = self.onMyPublish
        self.__mqtt_client.on_subscribe = self.onMySubscribe
    def onMyConnect(self,paho_mqtt, userdata,flags,rc):
        print(f"Client {self.__clientID} connected to {self.__brokerURL} with code {rc}")
    
    def onMyMessage(self,paho_mqtt,userdata,msg):
        self.notifier.notify(msg.topic,msg.payload)
    
    def onMyPublish(self,paho_mqtt,userdata,mid):
        print(f"Message with {mid} published")
    
    def onMySubscribe(self,paho_mqtt,userdata,mid,granted_qos):
        print(f"ID subscriber {mid}")
        
    def myPublish(self,topic,msg):
        self.__mqtt_client.publish(topic,json.dumps(msg),qos=2)
    
    def mySubscribe(self,topic):
        self.__topic = topic
        self.__isSubscriber = True
        
        self.__mqtt_client.subscribe(topic,qos=2)
        
        
    def start(self):
        self.__mqtt_client.connect(self.__brokerURL,self.__brokerPort)
        self.__mqtt_client.loop_start()
        
    def stop(self):
        if self.__isSubscriber:
            self.__mqtt_client.unsubscribe(self._topic)
        self.__mqtt_client.loop_stop()
        self.__mqtt_client.disconnect()
    
    