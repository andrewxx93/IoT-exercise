from myMQTT import *
import json
import time
import cherrypy

@cherrypy.expose
class OnlineLEDManager:
    def __init__(self,clientID,broker,port,topic):
        self.__topic = topic
        self.base_msg ={
            "bn": "OnlineLedManager",
            "e":[{
                "n": "Led1",
                "t": "",
                "u": "Boolean",
                "v": ""
            }]
        } 
        self.LedStatusPublisher = MyMQTT(clientID,broker,port)
        self.LedStatusPublisher.start()
    def GET(self):
        return open("index.html")
            
    
    def PUT(self,*uri):
        if len(uri)!=0:
            msg = self.base_msg
            msg["e"][0]["v"]=list(uri)[0]
            msg["e"][0]["t"] = str(time.time())
            self.LedStatusPublisher.myPublish(self.__topic,msg)
    

if __name__ == '__main__':
    with open("settings.json") as fp:
        settings = json.load(fp)
        
    conf = {
        '/':{
            'request.dispatch' :cherrypy.dispatch.MethodDispatcher()
        }
    }
    
    cherrypy.tree.mount(OnlineLEDManager("AndreaCataniaPub",settings["broker2"],settings["port"],settings["topic"]),'/',conf)
    cherrypy.engine.start()
    cherrypy.engine.block()
    