from myMQTT import * 
import json
import time


class LEDonline:
    def __init__(self,clientID,broker,port,topic):
        self.__clientID = clientID
        self.__brokerURL = broker
        self.__brokerPort = port
        self.__topic = topic
        
        #MQTT client creation
        self.ledOnline = MyMQTT(self.__clientID,self.__brokerURL,self.__brokerPort,self)
        
    def notify(self,topic,msg):
        print(f"{json.loads(msg)}")
        
    def start(self):
        self.ledOnline.start()
        self.ledOnline.mySubscribe(self.__topic)
        
    def stop(self):
        self.ledOnline.stop()
        

if __name__ == '__main__':
    with open("settings.json") as fp:
        settings = json.load(fp)
    
    
    led1 = LEDonline("LED1AndreaCTSub",settings["broker2"],settings["port"],settings["topic"])
    led1.start()
    while True:
        time.sleep(1)



    