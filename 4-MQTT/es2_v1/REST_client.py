
import json
import time
from ledManager import *
import cherrypy


class RESTLedManager(object):
    exposed = True
    def __init__(self):
        with open("settings.json") as fp:
            broker_conf = json.load(fp)
            
        broker = broker_conf["broker"]
        port = broker_conf["port"]
        clientID = "RESTLEDManagerAndreaCT"
        topic = broker_conf["topic"]
        self.led_client = LedManager(clientID,broker,port,topic)
        self.led_client.start()
        
    def GET(self):
        return open("index.html")
    
    def PUT(self,*uri):
        if list(uri)[0] != 0:
            print(type(list(uri)[0] ))
            self.led_client.publish(list(uri)[0])
            
            
if __name__ == "__main__":
    conf = {
        "/":
            {
                "request.dispatch": cherrypy.dispatch.MethodDispatcher()
            }
    }
    cherrypy.quickstart(RESTLedManager(),"/",conf)
    
    