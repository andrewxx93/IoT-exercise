import paho.mqtt.client as PahoMQTT
import time
import json


class LEDManager:
    def __init__(self,clientID,broker,port):
        self.__clientID = clientID
        self.__brokerURL = broker
        self.__brokerPort = port
        
        #MQTT client creation
        self.__mqtt_client = PahoMQTT.Client(self.__clientID,False)
        
        #Callbacks registration
        self.__mqtt_client.on_connect = self.onMyConnect
        self.__mqtt_client.on_publish = self.onMyPublish
        
    
    def start(self):
        self.__mqtt_client.connect(self.__brokerURL,self.__brokerPort)
        self.__mqtt_client.loop_start() 
    def stop(self):
        self.__mqtt_client.loop_stop()
        self.__mqtt_client.disconnect()
        
    
    def onMyConnect(self,paho_mqtt,userdata,flags,rc):
        print(f"Client {self.__clientID} connected to {self.__brokerURL} with code {rc}")
    
    def onMyPublish(self,paho_mqtt,userdata,mid):
        print(f"Message with id: {mid} correctly published")   
        
    def publishStatus(self,topic,status,qos=2):
        msg= {
            "bn":"LedManager",
            "e":[
                {
                "n":"Led1",
                "u": "Boolean",
                "t": str(time.time()),
                "v": status
            }]
            }

        self.__mqtt_client.publish(topic,json.dumps(msg),qos)
        
        

if __name__ == '__main__':
    with open("settings.json") as fp:
        conf = json.load(fp)
    
    lm1 = LEDManager("AndreaCataniaPub",conf["broker2"],conf["port"])
    lm1.start()
    time.sleep(1)
    text = "0->turn off\n1-turn on\nq->exit\n"
    command = ""
    status = ""
    topic = "IoT/AndreaCatania/led"
    
    while True:
        command=input(text)
        if command == '0':
            status = 'off'
            lm1.publishStatus(topic,status,qos=2)
        elif command =='1':
            status = 'on'
            lm1.publishStatus(topic,status,qos=2)
        elif command == 'q':
            break
        else:
            print("Invalid command")
            