import paho.mqtt.client as PahoMQTT
import json
import time 

class MySubscriber:
    def __init__(self,clientID,broker,port,topic):
        self.__clientID = clientID
        self.__brokerURL = broker
        self.__brokerPort = port
        self.__topic = topic
        self.__mqtt_client = PahoMQTT.Client(self.__clientID,False)
        
        #Callback registrations
        self.__mqtt_client.on_connect = self.myOnConnect
        self.__mqtt_client.on_message = self.myOnMessage
    
    def start(self):
        self.__mqtt_client.connect(self.__brokerURL,self.__brokerPort)
        self.__mqtt_client.loop_start()
        self.__mqtt_client.subscribe(self.__topic,qos=2)
        
    def stop(self):
        self.__mqtt_client.unsubscribe(self.__topic)
        self.__mqtt_client.loop_stop()
        self.__mqtt_client.disconnect()
    
    def myOnConnect(self, paho_mqtt, userdata, flags, rc):
        print(f"Client {self.__clientID} connected to {self.__brokerURL} with code {rc}")
        
    def myOnMessage(self,paho_mqtt,userdata,msg):
        print(f"Topic: {msg.topic}, QoS: {msg.qos}, Message: {msg.payload.decode('utf-8')}")
        
    
if __name__ == '__main__':
    with open("settings.json") as fp:
        settings = json.load(fp)
    topic = "Questa/prova/semplice"
    
    subscriber1 = MySubscriber("AndreaCTs280665Subscriber",settings["broker2"],settings["port"],topic)
    subscriber1.start()
    while True:
        time.sleep(1)
        