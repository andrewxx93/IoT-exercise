import paho.mqtt.client as PahoMQTT
import json
import time
import datetime
import random

class LED:
    def __init__(self,clientID,broker,port,topic):
        self.__clientID = clientID
        self.__brokerURL = broker
        self.__brokerPort = port
        self.__topic = topic
        self.led_status = None
        
        #MQTT client creation
        self.__mqtt_client = PahoMQTT.Client(self.__clientID,False)
        
        #Callback registration
        self.__mqtt_client.on_connect = self.myOnConnect
        self.__mqtt_client.on_message = self.setStatus #When a message is received the LED status will be updated

    def start(self):
        self.__mqtt_client.connect(self.__brokerURL,self.__brokerPort)
        self.__mqtt_client.loop_start()
        self.__mqtt_client.subscribe(self.__topic,qos = 2)
        
    def stop(self):
        self.__mqtt_client.unsubscribe(self.__topic)
        self.__mqtt_client.loop_stop()
        self.__mqtt_client.disconnect()
    
    def myOnConnect(self,paho_mqtt,userdata,flags,rc):
        print(f"Client {self.__clientID} connected to the broker {self.__brokerURL} with code {rc}")
        
    def setStatus(self,paho_mqtt,userdata,msg):
        #The message received will be in the SenML format. For this reason we have to convert into a dict before access
        #to the content.
        msg = json.loads(msg.payload)
        self.led_status = msg["e"][0]["v"]
        timestamp = msg["e"][0]["t"]
        print(f"The LED is set to: {self.led_status} at {timestamp}")
        
    
    
if __name__ == '__main__':
    with open("settings.json") as fp:
        conf = json.load(fp)
        
    #led1 = LED("AndreaCTSub",conf["broker2"],conf["port"],"IoT/AndreaCatania/led")  #For exercise 1
    led1 = LED("AndreaCTSub",conf["broker2"],conf["port"],"Andrea/Catania/LedBlinking1") #For exercise 2
    led1.start()
    
    while True:
        time.sleep(1)
        