import paho.mqtt.client as PahoMQTT
import json
import random
import time
class MyPublisher:
    def __init__(self,clientID,broker,port):
        self.__clientID = clientID
        self.__brokerURL = broker
        self.__brokerPort = port
        self.__mqtt_client = PahoMQTT.Client(self.__clientID,False) #A new MQTT client is created
        
        #Callback registration: this is needed in order to execute our function when the relative Acknowledgment arrives, 
        #for instance a CONNACK
        self.__mqtt_client.on_connect = self.myOnConnect
        self.__mqtt_client.on_publish = self.myOnPublish
        self.__mqtt_client.on_disconnect = self.myOnDisconnect
        
    def start(self):
        #Starting the connection with the broker:first we have to connect to the broker and the we start the loop
        self.__mqtt_client.connect(self.__brokerURL,self.__brokerPort)
        self.__mqtt_client.loop_start()
        
    def stop(self):
        #Stopping the connectio with the broker: first we have to stoop the loop and then we can disconnect from the broker
        self.__mqtt_client.loop_stop()
        self.__mqtt_client.disconnect()
    
    #Callbacks definition
    
    def myOnConnect(self,paho_mqtt,userdata,flags,rc):
        print(f"Connected to {self.__brokerURL} with result code {rc} ")
    
    def myOnPublish(self,paho_mqtt,userdata,mid):
        print(f"The message with id {mid} has been sent")
        
    def myOnDisconnect(self,paho_mqtt,userdata,rc):
        print(f"Disconnected from {self.__brokerURL} with result code {rc} ")

        
    def myPublish(self,topic,message,qos = 2):
        self.__mqtt_client.publish(topic,message,qos)
        
        


if __name__ == '__main__':
    with open("settings.json") as fp:
        settings = json.load(fp)
    command = ""
    publisher1 = MyPublisher("AndreaCTs280665Publisher",settings["broker2"],settings["port"])
    topic = "Questa/prova/semplice"
    
    publisher1.start()
    while True:
        message = random.randint(0,100)
        publisher1.myPublish(topic,message)
        time.sleep(1)
        