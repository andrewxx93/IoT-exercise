from MyMqtt_general_client import * 
import json
import time


class LedManager():
    def __init__(self,clientID,broker,port,topic):
        self.topic = topic
        self.broker = broker
        self.port = port
        self.__message ={'clientID':clientID,'n':'led','status':None,'timestamp':'','unit':'boolean'}
        self.client = MyMQTT(clientID,broker,port)
        
    def start(self):
        #print(f"Starting connection to the broker '{self.broker}' at port'{self.port}'")
        self.client.start()
        
    def stop(self):
        #print(f"Stopping connection to the broker '{self.broker}'")
        self.client.stop()
        
    def publish_status(self,status_value,qos=2):
        message = self.__message
        message['status'] = status_value
        #time.localtime() # get struct_time
        message['timestamp'] = time.strftime("%m/%d/%Y, %H:%M:%S", time.localtime())
        self.client.myPublish(self.topic,message,qos)
        
        
        
if __name__ == '__main__':
    with open("settings.json") as fp:
        print("Importing the configuration for the connection to  the broker")
        settings = json.load(fp)
    
    broker_address = settings['broker']
    broker_port = settings['port']
    led_manager = LedManager("myLED121212", broker_address, broker_port,"IoT/AndreaCT/led")
    
    led_manager.start()

    done = False
    
    information = "Write on/off to turn on/off the led otherwise q to exit"
    while not done:
        time.sleep(0.5)
        print("\n\n"+information)
        user_command = input("Insert your choice: ")
        if user_command == "on" or user_command == "off":
            led_manager.publish_status(user_command)
        elif user_command == "q":
            led_manager.stop()
            done = True
        else:
            print("Unknown command!Try again")
    
    print("See you next time")
        