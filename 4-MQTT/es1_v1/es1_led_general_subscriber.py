from MyMqtt_general_client import *
import json
import time


class Led():
    def __init__(self,clientID,broker, port, topic):
        self.client = MyMQTT(clientID, broker, port, self)
        self.topic = topic
        self.status = None
        
    def start(self):
        print("Starting mannaggiaddio")
        self.client.start()
        self.client.mySubscriber(self.topic,qos=2)
    
    def stop(self):
        self.client.stop()
        
    def notify(self,topic,msg):
        payload = json.loads(msg)
        clientID = payload['clientID']
        timestamp = payload['timestamp']
        self.status = payload['status']
        print(f"\nSet LED to {self.status} by {clientID} at {timestamp}")
        
    def disconnect(self):
        print("disconnetion")
        self.client.disconnect()
    
    def reconnect(self):
        print("reconnection")
        self.client.reconnect()
        
if __name__ == '__main__':
    with open("settings.json") as fp:
        settings = json.load(fp)
    
    broker_address = settings["broker"]
    broker_port = settings["port"]
    
    led = Led("led_subscriber_1234", broker_address, broker_port, "IoT/AndreaCT/led" )
    
    done = False
    
    led.start()
    
    while not done:
        time.sleep(3)
        
        user_command = input("Insert q to unsubscribe: ")
        
        if user_command == "q":
            done = True
            
    led.stop()    
        
    