from generalClient import * 
import json
import time

class Led(object):
    def __init__(self,clientID,broker,port,topic):
        self.client = MyMQTT(clientID, broker, port,self)
        self.topic = topic
        self.led_status = None
        
    def start(self):
        print("starting")
        self.client.start()
        print(self.topic)
        self.client.mySubscriber(self.topic)
    
    def stop(self): 
        print("stopping")   
        self.client.stop()
            
    def notify(self,topic, payload):
        payload = json.loads(payload)
        self.led_status = payload["status"]
        timestamp = payload["timestamp"]
        pubID = payload["publisherID"]
        print(f"The status of the led is set to {self.led_status} by {pubID} at {timestamp}")

if __name__ == "__main__":
    with open("settings.json") as fp:
            message_broker_settings = json.load(fp)
    broker = message_broker_settings["broker"]
    port = message_broker_settings["port"]
    topic = message_broker_settings["topic"]
    led1 = Led("AndreaCatania18041993subscriber",broker,port,topic)
    led1.start()

    
    while True:
        time.sleep(1)
    
    led1.stop()
    #Message shape
    """ 
    {
        "status" : "(on|off)",
        "timestamp": value,
        "publisherID": value 
        
    }
    
    
    """
    