from generalClient import *
import time 
import json
import random


class LedManager(object):
    def __init__(self,clientID,broker,port,topic):
        self.client = MyMQTT(clientID, broker, port)
        self.topic = topic
        self.message = {"status": None,"timestamp":None, "publisherID":clientID}
        
    
    def start(self):
        self.client.start()
        
    def stop(self):
        self.client.stop()
        
    def publish(self,led_status_value):
        message = self.message
        message["status"] = led_status_value
        message["timestamp"] = time.time()
        self.client.myPublish(topic, message)
        
        
if __name__ == "__main__":
    with open("settings.json") as fp:
        broker_settings = json.load(fp)
    clientID = "AndreaCatania18041993publisher"
    broker = broker_settings["broker"]
    port = broker_settings["port"]
    topic = broker_settings["topic"]
    led_pub = LedManager(clientID, broker, port, topic)
    
    led_pub.start()
    
    counter = 10
    
    while counter:
        
        if random.randint(1,10) % 2 == 0:
            led_pub.publish("on")
        else:
            led_pub.publish("off")
        
        time.sleep(1)
        counter -= 1
        
    led_pub.stop()   





#Message shape
    """ 
    {
        "status" : "(on|off)",
        "timestamp": value,
        "publisherID": value 
        
    }
    
    
    """