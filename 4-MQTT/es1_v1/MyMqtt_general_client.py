import paho.mqtt.client as PahoMQTT
import json
import time

class MyMQTT():
    
    #Notifier is used to pass to an instace of a class MyMQTT something to be done
    #when the general client is a subscriber such that when a message is received
    #a certain operation is performed (for instance print the value or do some computation)
    
    def __init__(self,clientID, broker, port, notifier = None):
        
        self.broker = broker
        self.port = port
        self.clientID = clientID
        self.notifier = notifier
        
        self.__topic = ""
        self._isSubscribed = False
        
        
        #create an instance of paho.mqtt.client library
        
        self._paho_mqtt = PahoMQTT.Client(clientID,clean_session = False)   
        
        #Registering the callbacks
        self._paho_mqtt.on_connect = self.myOnConnect            #callback to myOnConnect
        self._paho_mqtt.on_message = self.myOnMessageReceived    #callback to myOnMessageReceived
        
    def myOnConnect(self,paho_mqtt,userdata,flags,rc):
        print("Connected to %s with result code: %d" %(self.broker,rc))
    
    def myOnMessageReceived(self, paho_mqtt,userdata, msg):
        self.notifier.notify(msg.topic,msg.payload)
        
    def myPublish(self,topic,msg,qos=2):
        print("Publishing\n\n'%s'\n\nwith topic '%s' and with quality of service '%d'" %(msg,topic,qos))
        self._paho_mqtt.publish(topic,json.dumps(msg),qos)
        
    def mySubscriber(self,topic,qos=2):
        print("Subscribing to '%s' with qos '%d'" %(topic,qos))
        
        self._paho_mqtt.subscribe(topic,qos)
        
        #This is used to rembember that it works also as a subscriber
        self._isSubscribed = True
        self._topic = topic
    
    def start(self):
        print("Starting connection")
        self._paho_mqtt.connect(self.broker,self.port)
        print("Connected")
        self._paho_mqtt.loop_start()
        print("Loop started")
        
    def stop(self):
        if self._isSubscribed:
            #We have to rembember that we have to unsubscribe if it is working as a subscriber
            self._paho_mqtt.unsubscribe(self._topic)
            
        self._paho_mqtt.loop_stop()
        self._paho_mqtt.disconnect()
    

        