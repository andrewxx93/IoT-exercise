import paho.mqtt.client as PahoMQTT
import json
import time


class Controller:
    def __init__(self,clientID,broker,port,topic1,topic2):
        self.clientID = clientID
        self.brokerURL = broker
        self.brokerPort = port
        self.topic1 = topic1
        self.topic2 = topic2
        
        #MQTT client creation
        self.lightControl = PahoMQTT.Client(self.clientID,False)
        
        #Callbacks registrations
        
        self.lightControl.on_connect = self.myOnConnect
        self.lightControl.on_message = self.myOnMessage
        self.lightControl.on_publish = self.myOnPublish
        
    def start(self):
        self.lightControl.connect(self.brokerURL,self.brokerPort)
        self.lightControl.loop_start()
        self.lightControl.subscribe(self.topic1,qos=2)
        
    def stop(self):
        self.unsubscribe(self.topic1)
        self.lightControl.loop_stop()
        self.lightControl.disconnect()
        
    def myOnConnect(self,paho_mqtt,userdata,flags,rc):
        print(f"Connected with status {rc}")
        
    def myOnMessage(self,paho_mqtt,userdata,msg):
        #Here we have to take the message published on topic1, check the value and if the condition
        #is true we have to publish the information on the topic2
        #print(msg.payload)
        msg = json.loads(msg.payload)
        print(msg)
        if int(msg["e"][0]["v"])< 75:
            correction = 100 - int(msg["e"][0]["v"])
            correction_msg = {
                "bn": "Controller",
                "e":[{
                    "n": "LightController",
                    "t": str(time.time()),
                    "v" : str(correction),
                    "u": "lux"
                }]
            }
            print(correction_msg)
            self.lightControl.publish(self.topic2,json.dumps(correction_msg))
    
    def myOnPublish(self,paho_mqtt,usrdata,mid):
        print(f"Message published with ID {mid}")
    

if __name__ == '__main__':
    with open("settings.json") as fp:
        conf = json.load(fp)
        
    LC = Controller("LighControllerAndreaCT",conf["broker2"],conf["port"],conf["topic1"],conf["topic2"])
    LC.start()
    
    while True:
        time.sleep(1)