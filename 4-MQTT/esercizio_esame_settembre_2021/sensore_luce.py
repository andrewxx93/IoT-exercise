import paho.mqtt.client as PahoMQTT
import json
import time
import random

class Sensor:
    def __init__(self,clientID,broker,port):
        #MQTT client creation
        self.__mqtt_client = PahoMQTT.Client(clientID, False)
        self.brokerURL = broker
        self.brokerPort = port
        #Callbacks registration
        self.__mqtt_client.on_connect = self.myOnConnect
        self.__mqtt_client.on_publish = self.myOnPublish
        
        self.msg = {"bn":"LightSensor",
                    "e":[{
                        "n" :"1",
                        "t" : "",
                        "u": "lux",
                        "v" : ""
                    }]}
        
    def myOnConnect(self,paho_mqtt,userdata,flags,rc):
        print(f"Connected to {self.brokerURL} with code {rc}")
    
    def myOnPublish(self,paho_mqtt,userdata,mid):
        print(f"Message published with ID {mid}")
    
    def start(self):
        self.__mqtt_client.connect(self.brokerURL,self.brokerPort)
        self.__mqtt_client.loop_start()
        
    def stop(self):
        self.__mqtt_client.loop_stop()
        self.__mqtt_client.disconnect()
        
    def myPublish(self,topic,value):
        msg = self.msg
        msg["e"][0]["v"] = str(value)
        msg["e"][0]["t"] = str(time.time())
        self.__mqtt_client.publish(topic,json.dumps(msg),qos=2)
        
    
if __name__ == '__main__':
    with open("settings.json") as fp:
        conf = json.load(fp)
    
    s1 = Sensor("Exam-Sensor1Publisher",conf["broker2"],conf["port"])
    s1.start()
    
    while True:
        s1.myPublish(conf["topic1"],random.randint(70,100))
        time.sleep(2)
    