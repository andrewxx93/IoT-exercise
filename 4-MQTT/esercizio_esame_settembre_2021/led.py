import paho.mqtt.client as PahoMQTT
import json
import time

class LED:
    def __init__(self,clientID,broker,port,topic2):
        self.brokerURL = broker
        self.brokerPort = port
        self.topic2 = topic2
        
        #MQTT client creation
        self.led = PahoMQTT.Client(clientID,False)
        
        #Callback registration
        self.led.on_connect= self.myOnConnect
        self.led.on_message =self.myOnMessage
        
    def start(self):
        self.led.connect(self.brokerURL,self.brokerPort)
        self.led.loop_start()
        self.led.subscribe(self.topic2,qos=2)
        
    def stop(self):
        self.led.unsubscribe(self.topic2)
        self.led.loop_stop()
        self.led.disconnect()
        
    def myOnConnect(self,paho_mqtt,userdata,flags,rc):
        print(f"Connected with code {rc}")
        
    def myOnMessage(self,paho_mqtt,userdata,msg):
        msg = json.loads(msg.payload)
        print(f'The correction value is: {msg["e"][0]["v"]}')
    
        
    

if __name__ == '__main__':
    with open("settings.json") as fp:
        conf = json.load(fp)
        
    led = LED("LedAndreaCatania",conf["broker2"],conf["port"],conf["topic2"])
    led.start()
    while True:
        time.sleep(1)