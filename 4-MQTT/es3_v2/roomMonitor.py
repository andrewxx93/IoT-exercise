from MyMQTT import *
import json
import time

class RoomMonitor(MyMQTT):
    def __init__(self,clientID,broker,port):
        self.__clientID = clientID
        self.__brokerURL = broker
        self.__brokerPort = port
        
        self.__roomMonitor = MyMQTT(self.__clientID,self.__brokerURL,self.__brokerPort,self)
        
    def notify(self,topic,payload):
        print(f"Subscribed to topic: {topic}")
        print(f"Received message:\n{json.loads(payload)}")
    
    def start(self):
        
        self.__roomMonitor.start()
        
    def stop(self):
        self.__roomMonitor.stop()
        
    def subscribe(self,topic):
        self.__roomMonitor.mySubscribe(topic)
        
    def myUnsubscribe(self,topic):
        self.__roomMonitor._paho_mqtt.unsubscribe(topic)
    

if __name__ == '__main__':
    message = "1)Data from all the sensors\n2)Data from all sensors in a single floor\n3)Data from all sensors in a single room\nq)exit\nu)Unsubscribe from all\nChoice: "
    
    with open("settings.json") as fp:
        conf = json.load(fp)
        
    RM = RoomMonitor("AndreaIoT280665",conf["broker"],conf["port"])
    RM.start()
    time.sleep(1)
    topics = []
    while True:
        command  = input(message)
        
        if command == '1':
            t = conf["baseTopic"]+"/#"
            if t not in topics:
                topics.append(t)
                RM.subscribe(t)
                
            while command != 'r':
                command = input("Inser r to unsubscribe and return to the previous menu")
                RM.myUnsubscribe(t)
        elif command == '2':
            floor = input("Insert floor: ")
            t = conf["baseTopic"]+f"/{floor}"+"/#"
            if t not in topics:
                topics.append(t)
                RM.subscribe(t)
            while command != 'r':
                command = input("Inser r to unsubscribe and return to the previous menu")
                RM.myUnsubscribe(t)
        elif command == '3':
            floor = input("Insert floor from 0 to 4: ")
            room = input("Insert room from 1 to 3: ")
            t = conf["baseTopic"]+f"/{floor}"+f"/{room}"+"/#"
            if t not in topics:
                topics.append(t)
                RM.subscribe(t)
            while command != 'r':
                command = input("Inser r to unsubscribe and return to the previous menu")
                RM.myUnsubscribe(t)
        elif command == 'u':
            for topic in topics:
                RM.myUnsubscribe(topic)
        elif command == 'q':
            for topic in topics:
                RM.myUnsubscribe(topic)
            break