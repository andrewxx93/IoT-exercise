from MyMQTT import *
import json
import time
import telepot
from telepot.loop import MessageLoop


class LedManagerBot():
    def __init__(self,telegramTokenAPI,clientID,broker,port,baseTopic):
        #TelegramBot configuration
        self.__telgramTokenAPI = telegramTokenAPI
        self.ledManagerBot = telepot.Bot(self.__telgramTokenAPI)
        print("Starting bot...")
        MessageLoop(self.ledManagerBot,{'chat':self.on_chat_message}).run_as_thread()
        print("Bot started")
        #MQTT configuration
        self.__clientID = clientID
        self.__broker = broker
        self.__port = port
        self.__baseTopic = baseTopic
        self.__led_publisher = MyMQTT(self.__clientID,self.__broker,self.__port,notifier=None)
        self.__led_publisher.start()

        #SenML message layout 
        self.__message = {
            
            'bn' : "TelegramBot",
            'e': [{'n' : 'switch','v' : '','t' : '','u' : 'bool'}]
            
        }
    
    def on_chat_message(self,msg):
        content_type, chat_type, chat_ID = telepot.glance(msg)
        message = msg['text']
        
        if message == "/switchon":
            payload = self.__message.copy()
            payload['e'][0]['v'] = 'on'
            payload['e'][0]['t'] = time.time()
            
            topic = str(self.__baseTopic) + "/Fondachelli/camera/led1" 
            qos = 2
            
            self.__led_publisher.myPublish(topic,payload,qos)
            self.ledManagerBot.sendMessage(chat_ID,text = "LED switched ON ")
        elif message == "/switchoff":
            payload = self.__message.copy()
            payload['e'][0]['v'] = 'off'
            payload['e'][0]['t'] = time.time()
            
            topic = str(self.__baseTopic) + "/Fondachelli/camera/led1" 
            qos = 2
            
            self.__led_publisher.myPublish(topic,payload,qos)
            self.ledManagerBot.sendMessage(chat_ID,text = "LED switched OFF ")
        else:
            self.ledManagerBot.sendMessage(chat_ID,text = "Unknown command... ")
            
if __name__ == '__main__':
    print("Welcome")
    with open("settings.json") as fp:
        conf = json.load(fp)
    
    telegramTokenAPI = conf["telegramTokenAPI"]
    clientID = "AndreaCTLedManagerBot"
    broker = conf["broker1"]
    port = conf["port1"]
    baseTopic = conf["baseTopic"]
    
    lmb = LedManagerBot(telegramTokenAPI,clientID,broker,port,baseTopic)
    
    while True :
        time.sleep(1)
        