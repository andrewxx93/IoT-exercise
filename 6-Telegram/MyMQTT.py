import paho.mqtt.client as PahoMQTT
import json
import time


class MyMQTT(object):
    def __init__(self,clientID,broker,port,notifier=None):
        self.clientID = clientID
        self.broker = broker
        self.port = port
        self.notifier = notifier
        
        self.__topic = ""
        self.isSubscriber = False
        
        self.__paho_client = PahoMQTT.Client(self.clientID,clean_session = False)
        
        #Callback registration
        
        self.__paho_client.on_connect = self.myOnConnect
        self.__paho_client.on_message = self.myOnMessage
        
    def start(self):
        self.__paho_client.connect(self.broker,self.port)
        self.__paho_client.loop_start()
    
    def stop(self):
        if self.isSubscriber == True:
            self.__paho_client.unsubscribe(self.__topic)
        
        self.__paho_client.loop_stop()
        self.__paho_client.disconnect()
    
    def myOnConnect(self,paho_mqtt,userdata,flags,rc):
        print(f"Client {self.clientID} connected to the broker {self.broker} with code {rc} at {time.time()}")    
        
    def myPublish(self,topic,message,qos=0):
        
        self.__paho_client.publish(topic, json.dumps(message),qos)
        print(f"Published {message} on topic {topic} with QoS = {qos} by {self.clientID} at {time.time()}")
        
    def mySubscriber(self,topic_tuple_list):
        
        self.isSubscriber = True
        self.__topic = topic_tuple_list
        self.__paho_client.subscribe(topic_tuple_list)     #Remember to pass a tuple with topic and qos. If we want
                                                                #to subscribe to several topics we have to create a list
                                                                #of tuples [(topic1,qos),(topic2,qos),...]
        print(f"Subscribed to topic {self.__topic} at {time.time()}")
        
    def myUnsubscriber(self,topic_tuple_list):
        self.__paho_client.unsubscribe(self.topic_tuple_list)
        print(f"Unsubscribed to topic {self.__topic} at {time.time()}")
        
    def myOnMessage(self,paho_mqtt,userdata,msg):
        self.notifier.notify(msg.topic,msg.payload)
        