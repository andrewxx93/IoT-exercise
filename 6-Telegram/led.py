from MyMQTT import *
import json
import time


class Led():
    def __init__(self):
        with open("settings.json") as fp:
            conf = json.load(fp)

        self.__broker = conf["broker1"]
        self.__port = conf["port1"]
        self.__clientID = "AndreaCatania18041993"
        self.__status = None
        
        self.__led_subscriber = MyMQTT(self.__clientID,self.__broker,self.__port, self)
        
        
    def start_client(self):
        self.__led_subscriber.start()
    
    def stop_client(self):
        self.__led_subscriber.stop()
    
    def subscribe(self,topic_tuple_list):
        print("Subscribing")
        self.__led_subscriber.mySubscriber(topic_tuple_list)
    
    def unsubscribe(self,topic_tuple_list):
        print("Unsubscribing")
        self.__led_subscriber.unsubscribe(topic_tuple_list)
    
    def notify(self,topic,payload):
        #Insert here the routine that read the value from the message coming from
        #the publisher in order to switch on or off the LED. Try to connect with the Bluetooth
        #lamp in the room !!!
        message = json.loads(payload)
        print(f"The message is {message}")
        

if __name__ == "__main__":
    
    led1 = Led()
    led1.start_client()
    led1.subscribe(("Andrea/Catania/prova/Fondachelli/camera/led1",2))
    
    command = ""
    while command != 'q':
        time.sleep(1)
        comman = input()
        
    led1.stop()