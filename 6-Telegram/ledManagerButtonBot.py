import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
import json 
import time
from MyMQTT import *
from random import randint

class LedManagerButtonBot(object):
    def __init__(self,telegramTokenAPI,clientID,broker,port,baseTopic):
        
        #Telegram configuration
        self.__telegramTokenAPI = telegramTokenAPI
        self.__ledManagerButtonBot = telepot.Bot(self.__telegramTokenAPI) #The Bot is created
        MessageLoop(self.__ledManagerButtonBot,{'chat': self.on_chat_message,'callback_query': self.on_callback_query}).run_as_thread()
        
        #MQTT configuration 
        self.__clientID = clientID
        self.__broker = broker
        self.__port = port
        self.__baseTopic = baseTopic
        #Creating an instance of publisher, in order to publish the message coming from the telegram bot
        #Since it will be a publisher the fourth argument (notifier) will have the default value None
        self.__ledStatusPublisher = MyMQTT(self.__clientID,self.__broker,self.__port)
        
        
        #Starting the Publisher
        self.__ledStatusPublisher.start()
        
        #SenML message layout
        self.__messageLayout = {
            'bn':'TelegramButtonBot',
            'e': [{'n':'switch','v':'','t':None,'unit':'bool'}]
        }
        
    def on_chat_message(self,msg):    
        content_type,chat_type,chat_ID = telepot.glance(msg)
        message = msg['text']
        if message == "/switch":
            buttons = [
                [InlineKeyboardButton(text = f'ON ', callback_data = f'on'),
                InlineKeyboardButton(text = f'OFF ', callback_data = f'off'),]
                ]
            keyboard = InlineKeyboardMarkup(inline_keyboard = buttons)
            self.__ledManagerButtonBot.sendMessage(chat_ID,text = 'What do you want to do', reply_markup = keyboard)
        else:
            self.__ledManagerButtonBot(chat_ID,text = "Invalid Command")
    
    def on_callback_query(self,msg):
        query_ID,chat_ID,query_data = telepot.glance(msg,flavor = 'callback_query')
        payload =self.__messageLayout.copy()
        payload['e'][0]['v'] = query_data
        payload['e'][0]['t'] = time.time()
        topic = str(self.__baseTopic) + "/Fondachelli/camera/led1"
        self.__ledStatusPublisher.myPublish(topic,payload)
        self.__ledManagerButtonBot.sendMessage(chat_ID,text = f"Led switched {query_data}")       

if __name__ == '__main__':
    with open("settings.json") as fp:
        conf = json.load(fp)
    #Telegram
    telegramToken = conf["telegramTokenAPI"]
    
    #MQTT
    broker = conf["broker1"]
    port = conf["port1"]
    baseTopic = conf["baseTopic"]
    baseClient = conf["baseClientID"]
    clientID = str(baseClient) + str(randint(1,1000))
    
    #Instance of the LedManager
    led_m_bot = LedManagerButtonBot(telegramToken,clientID,broker,port,baseTopic)
    
    
    while True:
        time.sleep(1)