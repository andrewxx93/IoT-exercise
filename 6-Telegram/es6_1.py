import cherrypy
import json
import time
import telepot
from telepot.loop import MessageLoop

@cherrypy.expose
class RESTBot(object):
    def __init__(self,telegramToken):
        self.__telegramToken = telegramToken
        self.__restbot = telepot.Bot(self.__telegramToken)
        MessageLoop(self.__restbot,{'chat':self.on_chat_message})
        
    def GET(self):
        return "<html align=center> <h1>Welcome to the notification center!</h1> </html>"
    def POST(self):
        body = json.loads(cherrypy.request.body.read())
        body["timestamp"] = time.time()
        return json.dumps(body,indent=4)     
    def on_chat_message(self,msg):
        pass

if __name__ == '__main__':
    with open("settings.json") as fp:
        telegram_conf = json.load(fp)
    telegramToken = telegram_conf["telegramTokenAPI"]
    conf = {
        '/':
            {
                'request.dispatch':cherrypy.dispatch.MethodDispatcher()
            }
            
    }
    
cherrypy.tree.mount(RESTBot(telegramToken),'/',conf)
cherrypy.engine.start()
cherrypy.engine.block()