import telepot
from telepot.loop import MessageLoop
import json
import time

class SimpleEchoBot():
    def __init__(self):
        with open("settings.json") as fp:
            conf = json.load(fp)
        
        self.__chat_ID = ""
        self.token = conf["telegramTokenAPI"]
        self.bot = telepot.Bot(token=self.token)
        MessageLoop(self.bot, {'chat':self.on_chat_message}).run_as_thread()
        
    def on_chat_message(self, msg):
        content_type, chat_type,self.__chat_ID = telepot.glance(msg)
        
        message = msg['text']
        if message =="/switchon":
            print(f"LED switched ON by {self.__chat_ID} with Telegram Bot")
            self.bot.sendMessage(self.__chat_ID,text = f"Led switched on by {self.__chat_ID}")
        elif message =="/switchoff":
            print(f"LED switched OFF by {self.__chat_ID} with Telegram Bot")
            self.bot.sendMessage(self.__chat_ID,text = f"Led switched on by {self.__chat_ID}")
        else:
            print("Unknown command")
            self.bot.sendMessage(self.__chat_ID,text = "Unknown command")
            
            
if __name__ == "__main__":
    myBot = SimpleEchoBot()
    command = ""
    while command != 'q':
        time.sleep(1)
        command = input()