from math import sqrt
class Point(object):
    def __init__(self,x,y):
        self.x = x
        self.y = y
    def __repr__(self):
        return f"The coordinates of the point are ({self.x},{self.y})"
    def distance(self,other):
        return sqrt((other.x-self.x)**2+(other.y-self.y)**2)
    def move(self,new_x,new_y):
        self.x = new_x
        self.y = new_y
