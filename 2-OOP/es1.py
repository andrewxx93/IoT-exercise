from es1_class import Point
if __name__ == "__main__":
    a = Point(7,1)
    b = Point(1,1)
    print(f"Starting position--->{a}")
    print(f"The distance between the point a and b is: {a.distance(b)}")
    a.move(2,2)
    print(f"After the move--->{a}")
    
    print(a)
    print(b)
