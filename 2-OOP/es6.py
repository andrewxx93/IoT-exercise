import json
class NBAstatistics(object):
    def __init__(self,file_name):
        self.file_name = file_name
        with open(self.file_name) as fp:
            self.file_content = json.load(fp)
    def get_player(self):
        return self.file_content["players"]
    def avg_heigth(self):
        players = self.get_player()
        heights_list = [p["hgt"]/39.37 for p in players]
        return sum(heights_list)/len(heights_list)
    def avg_weight(self):
        players = self.get_player()
        weights_list = [p["weight"]/2.2 for p in players]
        return sum(weights_list)/len(weights_list)
    def avg_age(self):
        players = self.get_player()
        ages_list = [2020-p["born"]["year"] for p in players]
        return sum(ages_list)/len(ages_list)
    
    def avg_rating(self):
        players = self.get_player()
        avg_rating = dict.fromkeys(players[0]["ratings"][0],0)  #The method dict.fromkeys is used to create a dictionary with
                                                                #with the specified keys and values. In this case the result will be
                                                                #a dictionary with keys the same of "ratings" and for values zero.
        num_players = len(players)
        keys_list = players[0]["ratings"][0].keys()
        for player in players:
            player_ratings = player["ratings"][0]
            for key in keys_list:
                avg_rating[key] += player_ratings[key]/num_players
        return avg_rating

        
if __name__ == '__main__':
    NBA = NBAstatistics("players.json")
    avg_heigth = NBA.avg_heigth()
    avg_weight = NBA.avg_weight()
    avg_age = NBA.avg_age()
    avg_rating = NBA.avg_rating()
    print(f"Average heigth: {avg_heigth:.2f} meters\n")
    print(f"Average weight: {avg_weight:.2f} kg\n")
    print(f"Average age: {avg_age:.2f} years\n")
    print("-------------------")
    print(f"Average ratings:")
    print("-------------------")
    
    for key,value in avg_rating.items():
        print(f"{key}: {value}")
        
    