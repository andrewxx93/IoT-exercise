from es3_deck import Deck
if __name__ == "__main__":
    d = Deck()
    while True:
        
        selection = input("""\
The available commands are:
d) Draw card from the deck
s) Shuffle the deck
q) Quit from the program
p) print Deck
""")
        if selection == 'd':
            n = int(input("How many cards do you want?: "))
            print(d.draw(n))
            
        elif selection == 's':
            if d.cards != []:
                d.shuffle()
                print(d.cards)
            else:
                print("Ops..the deck is empty")
                
        elif selection == 'p':
            if d.cards != []:
                print(d.cards)
            else:
                print("We are sorry...the deck is empty")
            
        elif selection == 'q':
            print("See you next time ;-)")
            break
        
        else:
            print("\nCommand not found, please check your input and try again using availabe command\n")
        