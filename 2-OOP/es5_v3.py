import json
from es4_v2 import Contact

class BookAddress(object):
    def __init__(self,file_name):
        #Carico la rubrica in memoria
        self.c = Contact(file_name)
        self.contactList = self.c.loadContact()
            
    def read_book_address(self):
        for contact in self.contactList:
            print(contact)
            
    def add_contact(self):
        print("Welcome to the add contact routine")
        new_name = input("Insert name: ")
        new_surname = input("Insert surname: ")
        new_email = input("Insert email: ")
        new_contact = {"name":new_name, "surname":new_surname, "email":new_email} #creo il dizionario per il nuovo contatto
        self.contactList.append(new_contact )
        
    def delete_contact(self):
        print("Welcome to the delete contact routine")
        del_name = input("Insert name: ")
        del_surname = input("Insert surname: ")
        flag = -1 
        for i in range(len(self.contactList)):
            if self.contactList[i]["name"] == del_name and self.contactList[i]["surname"] == del_surname:
                flag = i
        if flag != -1:
            self.contactList.pop(flag)
        self.read_book_address()
        
    def find_by_name(self):
        name = input("Insert name: ")
        for contact in self.contactList:
            if contact["name"] == name:
                print(f"{contact['name']} {contact['surname']} {contact['email']}")
                
    def update_contact(self):
        print("Welcome to the update contact routine")
        name = input("Insert name: ")
        surname = input("Insert surname: ")
        for contact in self.contactList:
            if contact["name"] == name and contact["surname"] == surname:
                contact["name"] = name
                contact["surname"] = surname
                contact["email"] = input("Inser new email: ")
        print(self.contactList) 
        
    def save_change(self):
        file_name = input("Insert the file name. If it is equal to the present one the file will be overwritten: ")
        with open(file_name,'w') as fp:
            json.dump({"contacts":self.contactList},fp,indent=4)
            

##############################

if __name__ == '__main__':
    b = BookAddress("contacts.json")
    while True:
        print("\n----------------------------")
        print("Welcome to the Rubrica menu")
        print("----------------------------")
        print("""
r) Read the rubrica
d) Delete contact
u) Update a contact
a) Add a new contact
f) Find a name
s) Save the changes
q) Quit
""")
        command = input("Insert the desired operation: ")
        if command == 'r':
            print("Reading the Rubrica...\n")
            b.read_book_address()
        elif command =='a':
            print("Adding...\n")
            b.add_contact()
        elif command =='d':
            print("Deleting...\n")
            b.delete_contact()
        elif command =='f':
            print("Finding...\n")
            b.find_by_name()
        elif command =='u':
            print("Updating...\n")
            b.update_contact()
        elif command =='s':
            print("Saving...")
            b.save_change()
        elif command =='q':
            print("Good Bye")
            break
        else:
            print("The command is not available...please insert a correct one")