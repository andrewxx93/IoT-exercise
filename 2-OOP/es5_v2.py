import json 
class Contact(object):
    def new_contact(self,name,surname,mail):
        self.name = name
        self.surname = surname
        self.mail = mail
        return {"name":self.name, "surname":self.surname, "mail":self.mail} #creo il dizionario per il nuovo contatto
    
class BookAddress(object):
    def __init__(self,file_name):
        #Carico la rubrica in memoria
        self.c = Contact()
        with open(file_name) as fp:
            d = json.load(fp)
        self.contactList = []
        for contact in d["contacts"]:
            self.contactList.append(self.c.new_contact(contact["name"],contact["surname"],contact["mail"]))
    def read_book_address(self):
        print(self.contactList)
    def add_contact(self):
        print("Welcome to the add contact routine")
        new_name = input("Insert name: ")
        new_surname = input("Insert surname: ")
        new_mail = input("Insert mail: ")
        self.contactList.append(self.c.new_contact(new_name,new_surname,new_mail))
    def delete_contact(self):
        print("Welcome to the delete contact routine")
        del_name = input("Insert name: ")
        del_surname = input("Insert surname: ")
        flag = -1 
        for i in range(len(self.contactList)):
            if self.contactList[i]["name"] == del_name and self.contactList[i]["surname"] == del_surname:
                flag = i
        if flag != -1:
            self.contactList.pop(flag)
        print(self.contactList)   
    def find_by_name(self):
        name = input("Insert name: ")
        for contact in self.contactList:
            if contact["name"] == name:
                print(f"{contact['name']} {contact['surname']} {contact['mail']}")
    def update_contact(self):
        print("Welcome to the update contact routine")
        name = input("Insert name: ")
        surname = input("Insert surname: ")
        for contact in self.contactList:
            if contact["name"] == name and contact["surname"] == surname:
                contact["name"] = name
                contact["surname"] = surname
                contact["mail"] = input("Inser new mail: ")
        print(self.contactList) 
    def save_change(self):
        file_name = input("Insert the file name. If it is equal to the present one the file will be overwritten: ")
        with open(file_name,'w') as fp:
            json.dump({"contacts":self.contactList},fp,indent=4)
            

##############################

if __name__ == '__main__':
    print("CRUD operation in the Rubrica")
    b = BookAddress("contacts.json")
    b.read_book_address()
    #b.add_contact()
    #b.read_book_address()
    #b.delete_contact()
    #b.find_by_name()
    #b.update_contact()
    b.save_change()