from math import sqrt
class SquareManager():
    def __init__(self,l):
        self.l = l
        
    def area(self):
        return self.l* self.l
    
    def perimeter(self):
        return self.l*4
    
    def diagonal(self):
        return self.l*sqrt(2)
        
        