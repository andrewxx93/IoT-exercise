from es0_class import SquareManager

if __name__=="__main__":
    sm = SquareManager(float(input("Insert the size of the side: ")))
    print(f"The area of the square with side {sm.l} is {sm.area()}")
    print(f"The perimeter of the square with side {sm.l} is {sm.perimeter()}")
    print(f"The diagonal of the square with side {sm.l} is {sm.diagonal()}")