import json

class Contact(object):
    def __init__(self,file_path):
        self.file_name = file_path
        self.contactList = []
        print(self.file_name)
        
    def loadContact(self):
        with open(self.file_name) as fp:
            d = json.load(fp)
        for contact in d["contacts"]:
            tempContact ={
                "name" : contact["name"],
                "surname": contact["surname"],
                "email": contact["email"]
            }
            self.contactList.append(tempContact)
        print(f"The contact list contains {len(self.contactList)} contacts")
        return self.contactList
        
if __name__ == '__main__':  

    c = Contact("contacts.json")
    c.loadContact()