import math 
from es1_class import Point
class Line(object):
    def __init__(self,m=0,q=0):
        self.m = m #angular coefficient
        self.q = q #intersection with y axis
        
    def line_from_points(self,point1,point2):
        self.m = (point2.y-point1.y)/(point2.x-point1.x)
        self.q = -point1.x * self.m + point1.y
        
    def __repr__(self):
        return (f"The coefficient of the Line are m = {self.m}, q = {self.q}")
    
    def distance(self,point):
        
        
        d = abs(-self.m*point.x + point.y - self.q)/math.sqrt(self.m**2+1)
        #d_slide = abs(point.y-self.m*point.x-self.q)/math.sqrt(self.m**2+self.q**2)
        return f"The distance from line to point is {d}"
    
    def intersection(self,other):
        if self.m == other.m:
            print("The line are parallel")
        else:
            x = (other.q-self.q)/(self.m-other.m)
            y = self.m*(other.q-self.q)/(self.m-other.m)+self.q
            return Point(x,y)
            
    