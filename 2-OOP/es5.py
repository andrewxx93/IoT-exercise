import json

class Contact(object):
    def __init__(self,name,surname,mail):
        self.name = name
        self.surname = surname
        self.mail = mail
    def __repr__(self):
        return "{},{},{}".format(self.name,self.surname,self.mail)
    
class Contact2(object):
    def __init__(self,name,surname,mail):
        self.name = name
        self.surname = surname
        self.mail = mail
    def new_contact(self):
        return {"name": self.name, "surname": self.surname, "mail": self.mail}
    
class BookAddress(object):
    def __init__(self,fileName):
        self.fileName= fileName
        #self.contactsList = []
        self.contactsList2 = []
        with open(self.fileName) as fp:
            td = json.load(fp)
            for contact in td["contacts"]:
                #self.contactsList.append(Contact(contact["name"],contact["surname"],contact["mail"]))
                self.contactsList2.append(contact)
                
            
    def show(self):
        #for contact in self.contactsList:
        #   print(contact)
        for contact in self.contactsList2:
            print(contact)
            
    def delete(self,name):
        save_i = -1
        for i in range(len(self.contactsList2)):
            if self.contactsList2[i]["name"]== name:
                print("Contact found")
                save_i = i        
        if save_i != -1:
            removed_contact = self.contactsList2.pop(save_i)
            print(f'The contact number {save_i+1} {removed_contact["name"]}, {removed_contact["surname"]}, {removed_contact["mail"]} has been removed')
        else:
            print("!!!---contact not found---!!!")
    def find(self,name):
        result = [contact for contact in self.contactsList2 if contact["name"]==name]
        print("The searching result are:\n")
        if result==[]:
            print("!!!---no element found---!!!")
        else:
            print(result)
    def update(self,name,surname):
        for contact in self.contactsList2:
            if contact["name"]==name and contact["surname"]==surname:
                contact["mail"]= input(f"Write the new email for contact {name} {surname}: ")
            
            
    def add(self,name,surname,mail):
        #self.contactsList.append(Contact(name,surname,mail))
        self.contactsList2.append(Contact2(name,surname,mail).new_contact())
        
    
    
    def save(self):
        with open('contacts_bis.json','w') as fp:
            td = {"contacts":self.contactsList2}
            json.dump(td,fp,indent=4)

        

if __name__ == "__main__":
    book = BookAddress("contacts_bis.json")
    print("Welcome to contact manager")
    message = """
The available commands are: 
s) Show all contacts
u) Update contact email
f) Find contact by name
d) Delete a contact by name
n) Add a new contact
q) Quit from contact manager
    """
    

    while True:
        print(message)
        selector = input()
        if selector == 's':
            book.show() 
        elif selector == 'u':
            name = input("Write the contact name whose email you want to update: ")
            surname = input("Write the contact surname whose email you want to update: ")
            book.update(name,surname)
        elif selector == 'f':
            name = input("Write the contact name you want to search: ")
            book.find(name)
        elif selector == 'd':
            name = input("Write the contact name you want to remove: ")
            book.delete(name)
        elif selector == 'n':
            name = input("Write the contact name you want to add: ")
            surname = input("Write the contact surname you want to add: ")
            mail = input("Write the contact mail you want to add: ")
            book.add(name,surname,mail)
        elif selector == 'q':
            book.save()
            break
        else:
            print("!!!---command not found---!!!")
        
