from es1_class import Point
from es2_class import Line

if __name__ == "__main__":
    l = Line(3,2)
    print(l)
    
    print("Aggiorno i punti a e b")
    a = Point(0,1)
    b = Point(4,2)
    l.line_from_points(a,b)
    print(a)
    print(b)
    print(l)
    
    l = Line(2,-1)
    a = Point(1,5)
    print(l.distance(a))
    
    m = Line(3,2)
    i = l.intersection(m)
    print(l)
    print(m)
    if i != None:
        print(i)
    