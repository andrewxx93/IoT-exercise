from es3_card import Card
import random as r
class Deck():
    
    def __init__(self):
        self.__card_suit = ['Hearts','Diamonds','Clubs','Spades']
        self.__card_value = ['A','1','2','3','4','5','6','7','8','9','J','Q','K']
        self.cards = [Card(s,v) for s in self.__card_suit for v in self.__card_value]
    
    def shuffle(self):
        r.shuffle(self.cards)
        
    def draw(self,n = 1):
        r.shuffle(self.cards) #Shuffle the deck before extracting the card
        if n > 0:
            if len(self.cards) != 0:
                if n <= len(self.cards):
                    drawn_cards = []
                    for i in range(n):
                        drawn_cards.append(self.cards.pop()) #This function extracts the required number of cards from the deck
                    return drawn_cards
                
                elif n > len(self.cards):  #When n is greater than the length of the deck it returns the available number of cards
                    return self.draw(len(self.cards))
                
            else:
                return "The deck is empy"
            
        else:
            print("Please, inserter a positive number !!!")
            
        
        
        