import json
class Contact(object):
    def __init__(self,fileName):
        self.fileName = fileName
        self.contactList = []
    def loadContact(self):
        with open(self.fileName) as fp:
            td = json.load(fp)
            for contact in td["contacts"]:
                newContact = {
                    "name":contact["name"],
                    "surname":contact["surname"],
                    "mail":contact["mail"]
                }
                self.contactList.append(newContact)
                
    def showContacts(self):
        for contact in self.contactList:
            print(contact)            
            

if __name__ == "__main__":
    c = Contact("contacts.json")
    c.loadContact()
    c.showContacts()