class Card():
    def __init__(self,suit,value):
        self.card_suit = suit   
        self.card_value = value
    def __repr__(self):
        return f"{self.card_value},{self.card_suit}"