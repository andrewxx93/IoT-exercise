from es1_class import *

if __name__=="__main__":
    while True:
        command = input("The Available command are:\n\n\
d) Distance between two points\n\
m) Move a point according to new coordinates\n\
q) Exit from the loop\n\n\
Insert your choice: ")
        
        
        if command == "d":
            x1 = float(input("Insert the x coordinate of the first point: "))
            y1 = float(input("Insert the y coordinate of the first point: "))
            x2 = float(input("Insert the x coordinate of the second point: "))
            y2 = float(input("Insert the y coordinate of the second point: "))
            P1 = Point(x1,y1)
            P2 = Point(x2,y2)
            
            print(P1.distance(P2))
            
        elif command == "m":
            x_new = float(input("Insert the new x coordinate of the point: "))
            y_new = float(input("Insert the new y coordinate of the point: "))
            print(P1.x)
            print(P1.y)
            P1.move(x_new,y_new)
            print(P1.x)
            print(P1.y)
            
            
        elif command == "q":
            break
        else:
            print("Command not found")
            
    print("See you soon!")
    
    