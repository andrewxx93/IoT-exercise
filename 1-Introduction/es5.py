if __name__== "__main__":
    add = "The content of the original file is:"
    definition = """
A computer file is a computer resource for recording data discretely in a
computer storage device. Just as words can be written 
to paper, so can information be written to a computer
file. Files can be edited and transferred through the 
internet on that particular computer system."""
    with open("original.txt","w") as f: #modalità write: se il file non esiste viene creato
                                        #se invece il file esiste viene sovrascritto del tutto
        f.write(definition)
    with open("original.txt",'a') as f: #Modalità append: permette di scrivere in coda al testo già presente
        f.write("\nQuesta e' una nuova linea")
        
    with open("original.txt","r") as f:
        text = f.read()
        print(text)

    with open("copy.txt","w") as f:
        text = add + text
        f.write(text)
        
    with open("copy.txt","r") as f:
        text = f.read()
        print('\n\n'+ text)    
    