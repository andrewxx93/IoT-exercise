def myAdd(a,b):
    print(a+b)
    
def mySub(a,b):
    print(a-b)
    
def myMult(a,b):
    print(a*b)

def myDiv(a,b):
    try:
        c = a/b
        print(c)
    except ZeroDivisionError:
        print("Division by zero not allowed")