import json
if __name__ == "__main__":
    personal_data = {
        "projectName" : "",
        "companyName" : "",
        "deviceList" : [] }
    
    dev_ID = input("Insert device ID: ")
    dev_name = input("Insert device name: ")
    dev_type = input("Insert device type: ")
    device = {"deviceID":dev_ID,"deviceName":dev_name,"deviceType":dev_type}
    personal_data["projectName"] = input("Insert project name: ")
    personal_data["companyName"] = input("Insert company name: ")
    personal_data["deviceList"].append(device)
    #This function creates the file and prints the value of the dictionary into it
    with open("personal_data.json","w") as f:
        print("Writing the file....")
        json.dump(personal_data,f,indent=4)
    #this function reads the json file and store it into d as a dictionary 
    with open("personal_data.json","r") as f:
        print("Reading the file....\n\n")
        d = json.load(f)
        
    for key,value in d.items():
        if isinstance(value, list) == True:
            print('------------------------------------------')
            for elem in value:
                for key,value in elem.items():
                    print(f"\t-{key}: {value}\n")
            print('------------------------------------------')
        else:
            print(f"{key}: {value}\n")
    
    