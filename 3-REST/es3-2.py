import cherrypy
import json
import os, os.path

#@cherrypy.expose()
class BodyGet(object):
    exposed = True
    def GET(self):
        
        return """<html>
        <head>
            <link href="/static/css/style.css" rel="stylesheet">
        </head>
        <body>""" + "<h1>" + "Hello to the PUT request" +"</h1>" + """ </body>
        </html>"""
        

    
    
    def PUT(self,*uri,**params):
        print('This is a PUT request')
        body = cherrypy.request.body.read()
        print(type(body))
        json_body = json.loads(body)
        print(type(json_body))
        
        return json.dumps(json_body,indent=4) 
        
        
if __name__ == '__main__':
    print(f"The absolute path is: {os.path.abspath(os.getcwd())}")
    conf = {
        '/': {
            'request.dispatch' : cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on' : True,
            'tools.staticdir.root' : os.path.abspath(os.getcwd())
            
        },
        '/static':{
            'tools.staticdir.on' : True,
            'tools.staticdir.dir' : './public'
        }
    }
    cherrypy.quickstart(BodyGet(),'/',conf)
    