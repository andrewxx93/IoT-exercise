import cherrypy

class ReverseWord():
    exposed=True

    def GET(self,*uri):
        my_string=""
        rev_string = ""
        if len(uri)!= 0:
            my_string = uri[0]
            rev_string = my_string[::-1]
        return rev_string
        
        
        
if __name__=="__main__":
    #Standard configuration to serve the url "localhost:8080"
    conf={
            '/':{
            'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
            'tool.session.on':True
            }   
    }
    
    cherrypy.config.update({'server.socket_port':8090})
    cherrypy.tree.mount(ReverseWord(),'/',conf)
    cherrypy.engine.start()
    cherrypy.engine.block()