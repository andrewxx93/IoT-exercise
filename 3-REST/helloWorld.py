import cherrypy
class HelloWorld(object):
    exposed=True

    def GET(self,*uri,**params):
        #Standard output
        output = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
"""
        
        output += "<h1>Hello World </h1>"

        #print(output)
        
        #Check the uri in the requests
        #<br> is just used to append the content in a new line
        #(<br> is the \n for HTML)
        
        if len(uri)!=0:
            #print(type(uri))
            uri_list = list(uri)
            
            for elem in uri_list:
                output+='<br>'+f'uri:  {elem}'
                #Check the parameters in the request
                #<br> is just used to append the content in a new line
                #(<br> is the \n for HTML)
            
            
        if params!={}:
            for key,value in params.items():
                output+='<br>' + f'{key}: {value}'
        
        return output + "</body> </html>"
        
        
if __name__=="__main__":
    #Standard configuration to serve the url "localhost:8080"
    conf={
            '/':{
            'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
            'tool.session.on':True
            }   
    }
    
    cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(HelloWorld(),'/',conf)
