import json
import requests
import time

class Bandsintown(object):
    def handler(self,command):
        if command == 'artist':
            self.getArtist(input("Insert the artist name separate by a space: "))            
        if command == 'history':
            self.getHistory()
    
    def getArtist(self,full_name):
        base_url = 'https://rest.bandsintown.com/artists/'
        full_name_list = full_name.split(' ')
        if len(full_name_list) > 1:
            i = 0
            while i < len(full_name_list):
                url = base_url + full_name_list[i] + '%20' + full_name_list[i+1]
                i+=2
            url = url + '?app_id=510'
        else:
            url = base_url + full_name_list[0] + '?app_id=510'
        print(url)
        r = requests.get(url)
        print(json.dumps(r.json(),indent=4))
        
    
if __name__ == '__main__':
        
    b = Bandsintown()
    while True:
        command = input("Available functions \n1)Search artist info \n2)exit\nInsert your choice: ")
        if int(command) == 1:
            b.handler('artist')
        elif int(command) == 2:
            break
        else:
            print("Unknown command!")
        

