import threading
import numpy as np
import time
import json
import random

N_CUSTOMERS = 30
N_DESKS = 5

CASH_DESKS = [i for i in range(N_DESKS) ]
#CUSTOMERS_TIME_LIST = [np.random.poisson(10) for i in range(N_CUSTOMERS)]
CUSTOMERS_TIME_LIST = [random.randint(1,15) for i in range(N_CUSTOMERS)]

DESK_COUNTER = dict([[i,0] for i in range(len(CASH_DESKS))])
BUSY_DESK = dict([[i,0] for i in range(len(CASH_DESKS))])

class Customer(threading.Thread):
    def __init__(self,customerID,servingTime,semaphore):
        threading.Thread.__init__(self)
        self.customerID = customerID
        self.servingTime = servingTime
        self.semaphore = semaphore
        
        
    def run(self):
        self.semaphore.acquire()
        serving_cash = CASH_DESKS.pop(0)
        DESK_COUNTER[serving_cash] +=1 #We increment the number of customer served by a certain cash desk
        BUSY_DESK[serving_cash] += self.servingTime #We add how much time a cash desk is busy
        print(f"Sleeping for {self.servingTime}")
        time.sleep(self.servingTime/10)
        
        CASH_DESKS.append(serving_cash)
        self.semaphore.release()
    
if __name__ == '__main__':
    customers = [] #Definition of an empty list of threads
    T_semaphore = threading.Semaphore(len(CASH_DESKS)) #We set the internal semaphore counter to the number of cash desks
    
    for i in range(N_CUSTOMERS):
        customers.append(Customer(i, CUSTOMERS_TIME_LIST[i] , T_semaphore))
        print(CUSTOMERS_TIME_LIST[i])
    start_time = time.time()
    
    for c in customers:
        c.start()
    for c in customers:
        c.join()
    
    end_time = time.time()
    TOTAL_SERVING_TIME = end_time-start_time
    AVG_PER_DESK = [BUSY_DESK[i]/DESK_COUNTER[i] for i in range(N_DESKS)]
    print(f"The total time needed to serve all the customers is: {TOTAL_SERVING_TIME}")
    print(f"Number of customer served by each desk\n{json.dumps(DESK_COUNTER,indent=4)}")
    print(f"Average per desk : {AVG_PER_DESK}")
    
        

