import threading 
import requests
import json
import time

class MyThread(threading.Thread):
    def __init__(self,threadID,url):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.url = url
    
    def run(self):
        r = requests.get(self.url)
        print(r.status_code)

if __name__ == '__main__':
    with open('top500.json','r') as fp:
        urls = json.load(fp)
        
    urls_subset = urls['websites'][0:10]
    print(urls_subset)
    
    start_for = time.time()
    
    try:
        for url in urls_subset:
            r = requests.get(str(url))
            print(r.status_code)
        # r = requests.get(urls_subset[10])
        # print(r.status_code)
    
    except:
        print("An error occured!")

    stop_for = time.time()
    
    execution_time_for = stop_for-start_for
    print(f"The execution time using the for loop is {execution_time_for}")
    
    

    threads = []
    
    for index ,url in enumerate(urls_subset):
        threads.append(MyThread(index,url))
        
    start_thread = time.time()
    for t in threads:
        t.start()
        
    for t in threads:
        t.join()
        
    stop_thread = time.time()
    execution_time_thread = stop_thread -start_thread 
    print(f"The execution time using the for thread is {execution_time_thread}")
    
    """ 
    In this case the Threads are faster then the for-loop. This because
    the for-loop waits the previouse response of the request befere 
    starting a new request, while with the multi-threads approach, each 
    thread send a request almost in paralell without waiting the previous requests.
    If we join the thread the amount of time is larger since before killing the main
    thread we have to wait that al the threads are finished. Without the
    join we have that the execution time is a lot smaller since each thread does not wait
    till the previous has finished.
    
    """