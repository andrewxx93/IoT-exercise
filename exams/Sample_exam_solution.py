import json
import cherrypy


#books_list = [{"title":"book1","year":1980},{"title":"book2","year":1970},{"title":"book3","year":1975},{"title":"book4","year":1960},{"title":"book5","year":2018}]

class BookSort(object):
    exposed = True
    def __init__(self):
        pass
        
    def POST(self,*uri):
        reverse = False
        #Check if the uri is empty. If is empty we sort the book in ascending
        if len(uri) != 0:
            if uri[0] == 'D':
            #Sorting the books in descending way
                reverse = True
        
        #The list of book is passed in the body of the POST request. Since it is send as string we have to parse the body in
        #a python dictionary
        books_list = json.loads(cherrypy.request.body.read())
        #With the list comprehension I extract the pubblication year and I store it in a list
        dates = [book["year"] for book in books_list] 
        #Using the built-in method sort of the list, the dates list is sorted accordingly to correct sorting order given through the default
        #parameter reverse
        dates.sort(reverse=reverse) 
        sorted_book_list = []
        #For each sorted date we find the book reading the key "year" of each dictionary inside the list and then the right book is appended it
        #desired order.
        for date in dates:
            for book in books_list:
                if book["year"] == date:
                    sorted_book_list.append(book)
        return json.dumps(sorted_book_list,indent=4)
if __name__ == "__main__":
    
    conf = {
        "/":{
            "request.dispatch" : cherrypy.dispatch.MethodDispatcher()
        }
            
    }
    cherrypy.quickstart(BookSort(),'/',conf)