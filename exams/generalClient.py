import paho.mqtt.client as PahoMQTT
import json

class MyMQTT(object):
    def __init__(self, clientID, broker, port, notifier = None):
        self.broker = broker
        self.port = port
        self.clientID = clientID
        self.notifier = notifier
        
        self._topic = ""
        self._isSubscriber = False
        
        self._paho_mqtt = PahoMQTT.Client(clientID,False)
        self._paho_mqtt.on_connect = self.myOnConnect
        self._paho_mqtt.on_message = self.myOnMessageReceived
        
        
    def myOnConnect(self,paho_mqtt,userdata,flags,rc):
        print(f"Connected to {self.broker} with code: {rc}")
        
    def myOnMessageReceived(self,paho_mqtt,userdata,msg):
        self.notifier.notify(msg.topic,msg.payload)
        
    def myPublisher(self,topic,msg,qos=2):
        print(f"Publishing {msg} with topic {topic} with QoS: {qos}")
        self._paho_mqtt.publish(topic,json.dumps(msg,indent=4),qos)
        
    def mySubscriber(self,topic,qos=2):
        print(f"Subscribing to topic {topic} with QoS: {qos}")
        self._paho_mqtt.subscribe(topic,qos)
        self._isSubscriber = True
        self._topic = topic
        
    def start(self):
        self._paho_mqtt.connect(self.broker,self.port)
        self._paho_mqtt.loop_start()
    
    def unsubscribe(self,topic):
        if self._isSubscriber == True:
            self._paho_mqtt.unsubscribe(topic)
    def stop(self):
        if self._isSubscriber == True:
            self._paho_mqtt.unsubscribe(self._topic)
        
        self._paho_mqtt.loop_stop()
        self._paho_mqtt.disconnect()
        
    