from generalClient import *
import time
import json
import threading
stop_thread = False

class MyThread(threading.Thread):
    def __init__(self,threadID,chrono):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.chrono = chrono
    def run(self):
        while True:
            global stop_thread
            if stop_thread == True:
                break
            self.chrono.publish("AndreaCTs280665/chronometer/time","time")
            time.sleep(5)

class Chronometer(object):
    def __init__(self,clientID,broker,port,baseTopic):
        self.client = MyMQTT(clientID,broker,port,self)
        self.baseTopic = baseTopic
        self.message = { "pubID": clientID, "operation":None,"interval":0}
        self.starting_time = 0
        
    def start(self):
        self.client.start()
    def stop(self):
        self.client.stop()
        
    def notify(self,topic,payload):
        payload = json.loads(payload)
        if topic == self.baseTopic+"/start" and payload["operation"] == 'start':
            print("I'm start topic")
            self.starting_time = time.time()
            
        if topic == self.baseTopic+"/stop" and payload["operation"] == 'stop':
            print("I'm stop topic")
            print(f"Interval: {time.time()-self.starting_time}")
            
            

            
    def subscribe(self,topic):
        self.client.mySubscriber(topic)
        
    def publish(self,topic,operation):
        message = self.message
        if operation == "time":
            message["operation"] = operation
            message["interval"] = time.time()-self.starting_time
            self.client.myPublisher(topic,message)
        if operation == "start":
            message["operation"] = operation
            message["interval"] = time.time()-self.starting_time
            self.client.myPublisher(topic,message)
        if operation == "stop":
            message["operation"] = operation
            message["interval"] = time.time()-self.starting_time
            self.client.myPublisher(topic,message)
        
if __name__ == "__main__":
    with open("settings.json") as fp:
        conf = json.load(fp)
    broker = conf["broker"]
    port = conf["port"]
    baseTopic = "AndreaCTs280665/chronometer"
    chrono = Chronometer("AndreaCTs280665", broker, port, baseTopic)
    
    chrono.start()
    
    
    command = ""
    while command != 'q':
        command = input()
        if command == 'r':
            chrono.subscribe(baseTopic+"/start")
            chrono.subscribe(baseTopic+"/stop")
            chrono.publish(baseTopic+"/start","start")
            thread = MyThread(1,chrono)
            stop_thread = False
            thread.start()
        if command == "e":
            chrono.publish(baseTopic+"/stop","stop")
            time.sleep(1)
            chrono.client.unsubscribe(baseTopic+"/start")
            chrono.client.unsubscribe(baseTopic+"/stop")
            stop_thread = True
    
        