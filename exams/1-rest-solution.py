import cherrypy
import json

#I suppose to pass the string to be compared as a json file with the following structure {"string1":"value1","string2":"value2"}
class StringCompare(object):
    exposed = True
    def __init__(self):
        pass
    
    def POST(self):
        #It's needed to parse the body content since it is sent as a string, so we we have to convert into a python dictionary
        strings = json.loads(cherrypy.request.body.read())
        
        string1 = strings["string1"]
        string2 = strings["string2"]
        longest = ""
        
        if result := (string1 == string2): #I use the warlus operator in order to compare the strings and store the boolean result in a single operation
            print()
            return f"string 1: {string1}\nstring 2: {string2}\nequal: {result}"
        
        else :
            if len(string1)>len(string2):
                longest = string1
            elif len(string2)>len(string1):
                longest = string2
            
            if longest != "":
                return f"string 1: {string1}\nstring 2: {string2}\nequal: {result}\nlongest: {longest} - length: {len(longest)}"
            else:   
                return f"string 1: {string1}\nstring 2: {string2}\nequal: {result}\nlongest: equal length - length: {len(string1)}"
if __name__ == "__main__":
    conf = {
        "/":{
            "request.dispatch": cherrypy.dispatch.MethodDispatcher() #In this way I tell to the cherrypy engine to map the method I define as the HTTP method
        }
    }
    
    cherrypy.quickstart(StringCompare(),'/',conf)